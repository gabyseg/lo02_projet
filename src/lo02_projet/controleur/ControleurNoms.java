package lo02_projet.controleur;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import lo02_projet.jeu.Jeu;
import lo02_projet.joueur.Joueur;
import lo02_projet.vue.VueJeu;

/**
 * classe permettant de rentrer les noms des joueurs et de choisir les règles
 * @author gabrielsega
 *
 */
public class ControleurNoms extends JPanel implements Observer{
	/**
	 * Label contenant les instructions du jeu
	 */
	private JLabel jInstructions;
	/**
	 * Tableau des entrées pour les noms
	 */
	private ArrayList<JTextField> listeJText;
	/**
	 * Sert à choisir la difficulté des bots
	 */
	private JComboBox<String> jc;
	/**
	 * Sert à choisir comment vont être compté les points
	 */
	private JComboBox<String> jcComptagePoints;
	/**
	 * Checkbox servant à changer les règles
	 */
	private JCheckBox jcheck;

	/**
	 * Constructeur permettant d'initialiser le controleur
	 * @param j instance de jeu à modifier
	 */
	public ControleurNoms(Jeu j) {
		// TODO Auto-generated constructor stub
		this.listeJText = new ArrayList<JTextField>();
		j.addObserver(this);

		//on va ajouter les champs pour les différents joueurs
		int totauxJoueurs = j.getNbReels() + j.getNbVirtuel();

		//on ajoute le layout ici
		this.setLayout(new GridLayout(totauxJoueurs+4, 1));

		//on créé le label
		this.jInstructions = new JLabel("Veuillez saisir les noms des joueurs");
		this.jInstructions.setFont(this.jInstructions.getFont().deriveFont(20.0f));
		this.jInstructions.setHorizontalAlignment(JLabel.CENTER);

		//on va demander ici la difficultée des bots
		JPanel difficulte = new JPanel();
		JLabel jdiffi = new JLabel("Difficulté des bots : ");
		//la liste des difficultés ici :
		String[] listeStr = {"Aléatoire","Invisible"}; 
		JComboBox<String> jcb = new JComboBox<String>(listeStr);
		this.jc = jcb;
		difficulte.add(jdiffi);
		difficulte.add(jcb);

		//on va demander ici pour le comptage de points
		JPanel comptage = new JPanel();
		JLabel jComptage = new JLabel("Comment voulez vous compter les points?");
		//la liste des difficultés ici :
		String[] listeStrComptage = {"Classique","Par la valeur des cartes"}; 
		JComboBox<String> jcbComptage = new JComboBox<String>(listeStrComptage);
		this.jcComptagePoints = jcbComptage;
		comptage.add(jComptage);
		comptage.add(jcbComptage);

		//on créé ici pour savoir si on rajoute une carte
		JPanel rajoutCarte = new JPanel();
		rajoutCarte.setLayout(new GridBagLayout());
		this.jcheck = new JCheckBox();
		rajoutCarte.add(this.jcheck);
		rajoutCarte.add(new JLabel("Cliquez pour ajouter une carte mystère : vous ne connaissez ni la couleur ni la valeur de cette carte"));

		//ici le bouton pour passer à la suite
		JButton jb = new JButton("Suivant");
		difficulte.add(jb);
		JPanel controleurNom = this;
		jb.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

				Collecteur c = Collecteur.getInstance();

				c.setAttendue(EntreeAttendue.STRATEGIE);
				//on dépose ici la stratégie
				if(jc.isEnabled()) {
					c.deposerStrategie((String) jc.getSelectedItem());
				}

				c.setAttendue(EntreeAttendue.AJOUT_CARTE);
				//oour l'ajout de nouvelles cartes
				if(jcheck.isEnabled()) {
					String aj = "Non";
					if(jcheck.isSelected()) {
						aj = "Oui";
					}
					c.deposerAjoutCarte(aj);
				}
				
				c.setAttendue(EntreeAttendue.COMPTAGE_POINTS);
				if(jcbComptage.isEnabled()) {
					String val = "Valeur";
					if(((String) jc.getSelectedItem()).equals("Classique")) {
						val = "Classique";
					}
					c.deposerComptage(val);
				}

				//on remet la bonne stratégie
				c.setAttendue(EntreeAttendue.NOM_JOUEUR);

				//ici on récupère tous les noms qui ont été rentrés
				for(JTextField jtxt : listeJText) {
					if(jtxt.isEnabled()) {
						c.deposerNomJoueur(jtxt.getText());
					}
				}
				//ici on efface le controleur
				//on récupère la fenêtre
				JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(controleurNom);
				//on efface tout
				topFrame.getContentPane().removeAll();
				//on créé la vue de la partie
				VueJeu vj;
				

				//on rafraichit
				SwingUtilities.updateComponentTreeUI(topFrame);
			}
		});

		//on ajoute tout
		this.add(this.jInstructions);
		//on va créer tous les champs
		for(int i=0; i<totauxJoueurs; i++) {
			//on définit le texte du JLabel
			String txt = " : Joueur virtuel";
			if(i<j.getNbReels())
				txt = " : Joueur réel";

			//nouveau Jpanel
			JPanel jp = new JPanel();
			//le champ
			JTextField jt = new JTextField();
			jt.setPreferredSize(new Dimension(200, 20));
			//le label qui va avec
			JLabel jl = new JLabel(txt);
			jp.add(jt);
			jp.add(jl);
			this.add(jp);
			this.listeJText.add(jt);
		}

		this.add(rajoutCarte);
		this.add(comptage);
		this.add(difficulte);
	}

	/**
	 * Méthode qui permet de désactiver la jcombobox de la difficulté
	 */
	public void desactiverDifficulte() {
		this.jc.setEnabled(false);
	}
	
	/**
	 * Méthode qui permet de désactiver la jcombobox du comptage de points
	 */
	public void desactiverCompteur() {
		this.jcComptagePoints.setEnabled(false);
	}

	/**
	 * Méthode servant à désactiver l'ajout de la jcombobox
	 * @param check true si on veut cocher la case
	 */
	public void desactiverAjout(boolean check) {
		this.jcheck.setSelected(check);
		this.jcheck.setEnabled(false);
	}

	@Override
	public void repaint() {
		// TODO Auto-generated method stub
		super.repaint();
		if(this.jInstructions != null) {
			this.jInstructions.setFont(this.jInstructions.getFont().deriveFont(20.0f));
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		//on vérifie que c'est bien le jeu
		if(o instanceof Jeu) {
			Jeu j = (Jeu)o;
			//on désactive autant de JTextField que de joueurs déjà présents
			ArrayList<Joueur> arj = j.getJoueurs();
			for(int i=0; i<arj.size(); i++) {
				JTextField jtxt = this.listeJText.get(i);
				jtxt.setEnabled(false);
				jtxt.setText(arj.get(i).getNom());
			}
		}
	}
}
