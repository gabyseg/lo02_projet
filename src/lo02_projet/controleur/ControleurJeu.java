package lo02_projet.controleur;

import java.util.ArrayList;
import java.util.ListIterator;

import lo02_projet.cartes.Carte;
import lo02_projet.jeu.Jeu;
import lo02_projet.joueur.Joueur;
import lo02_projet.joueur.Virtuel;

/**
 * Classe permettant de controler le jeu
 * @author gabrielsega
 *
 *
 * Controleur du jeu
 *
 */
public class ControleurJeu{
	
	/**
	 * Jeu en cours
	 */
	private Jeu jeu;
	
	/**
	 * Lecteur de la console
	 */
	private LecteurConsole l;

	/**
	 * Constructeur du jeu
	 * @param j Jeu en cours
	 * @param l Lecteur de console
	 */
	public ControleurJeu(Jeu j) {
		// TODO Auto-generated constructor stub
		this.jeu = j;
	}

	/**
	 * Methode permettant de lancer la partie
	 * @throws Exception
	 */
	public void effectuerPartie() throws Exception {
		boolean fini = false;
		while(fini == false) {
			this.effectuerUnTour();
			this.jeu.estFini();
			fini = this.jeu.getEstFini();
		}

		//on attribue les trophés
		this.jeu.attribuerTrophees();
	}

	/**
	 * Methode permettant d'effectuer un tour de jeu
	 * @throws Exception
	 */
	public void effectuerUnTour() throws Exception {
		//on affiche le jeu

		//puis on regarde si le jeu est fini
		if(this.jeu.getPioche().getCartes().size() == 0 && this.jeu.getCartesRestantes().size() == 0 ) {
			//si oui il récupère sa dernière carte
			for(Joueur j : jeu.getJoueurs())
				j.prendreDerniereCarte();
		}else {
			//on commence par distribuer les cartes
			jeu.completer();
			jeu.distribuer(2);
			//on commence par les offres
			this.offres();
			//puis ils jouent
			this.prendreCartes();

			//sinon on ramasse les offres et on recommence
			if(jeu.getPioche().getCartes().size() != 0) {
				jeu.recupererOffres();
			}
		}

	}

	/**
	 * Methode permettant le lancement des offres (les joueurs selectionnent la carte qu'ils souhaitent rendre visible)
	 */
	public void offres() {
		Collecteur c = Collecteur.getInstance();
		for(Joueur j : this.jeu.getJoueurs()) {
			if(j.estReel()) {
				// Récupération des cartes du joueur
				Carte[] offre = j.getOffre();

				// On vérifie que le joueur possède bien deux cartes
				if (offre[0] != null && offre[1] != null) {
					//on dit que le joueur en cours est le suivant
					this.jeu.setJoueurEnCours(j);
					// Demande au joueur quelle carte il souhaite retourner
					
					// Récupération du choix
					c.setAttendue(EntreeAttendue.NUMERO_OFFRE);
					int choix = c.lireNumeroDansOffre();
					
					// Vérification de la conformité du choix
					while (choix != 1 && choix != 2) {
						//jeu.setMessageAAfficher("Veuillez rentrer le chiffre 1 ou 2 pour faire votre choix");
						choix = c.lireNumeroDansOffre();
					}

					// Changement de la visibilité de la carte choisi
					if (choix == 1) {
						offre[0].setVisible(true);
					} else if (choix == 2) {
						offre[1].setVisible(true);
					}
				}
			}else {
				Virtuel v = (Virtuel)j;
				v.faireOffre();
			}
		}

		//on indique que personne ne fait d'offre
		this.jeu.setJoueurEnCours(null);
	}

	/**
	 * Methode permettant de definir l'ordre des joueurs et de les faire prendre une carte dans une offre disponible
	 * @throws Exception
	 */
	public void prendreCartes() throws Exception {
		//on commence par définir le premier joueur
		Joueur j = jeu.definirPremierJoueur();
		j.setaDejaJoue(true);

		//le joueur prend une première carte
		Joueur suivant = this.fairePrendreCarte(j, jeu.getJoueursPiochables());

		//tant que tout le monde n'a pas joué
		while(jeu.ontPasJoue()!=0) {
			suivant.setaDejaJoue(true);
			suivant = this.fairePrendreCarte(suivant, jeu.getJoueursPiochables());

			//on vérifie que le nouveau suivant n'a pas déjà joué
			if(suivant.isaDejaJoue()) {
				suivant = jeu.definirPremierJoueurPasDejaJoue();//s'il a déjà joué on prend le premier qui ne l'a pas fait
			}
		}


		//on remet tous leurs tours à 0
		jeu.reinitialiserJoueurs();
	}

	/**
	 * Methode permettant de definir les trophes de la partie
	 */
	public void definirTrophees() {
		//on va tirer deux cartes
		int nbTirer = 2;
		if(jeu.getNbReels()+jeu.getNbVirtuel() == 4)
			nbTirer--;
		jeu.getTrophees().addAll(jeu.getPioche().tirer(nbTirer));
	}

	/**
	 * Methode initialisant le jeu
	 */
	public void initialiserjeu() {
		this.definirTrophees();
		//this.completer();
		//this.distribuer(2);
	}

/**
	 * Methode permettant a un joueur de prendre une carte dans une offre
	 * @param jQuiPioche Joueur qui doit prendre une carte
	 * @param liste Liste des joueurs disponibles
	 * @return Retourne le joueur auquel une carte a ete prise
	 * @throws Exception
	 */
	public Joueur fairePrendreCarte(Joueur jQuiPioche, ArrayList<Joueur> liste) throws Exception {
		jQuiPioche.setEstEnCoursDeJeu(true);
		Collecteur c = Collecteur.getInstance();
		if(jQuiPioche.estReel()) {
			if(liste.size() != 1)
				liste.remove(jQuiPioche);
			
			//création du joueur
			Joueur j = null;
			
			//tant qu'on a pas de joueur
			while(j == null) {
				//on demande de choisir

				// Récupération du nom du joueur choisi
				c.setAttendue(EntreeAttendue.NOM_JOUEUR_PRENDRE_CARTE);
				String nomJoueur = c.lireNomJoueur();
				
				//on cherche le joueur
				ListIterator<Joueur> it = liste.listIterator();
				while(it.hasNext() && j == null) {
					Joueur temp = it.next();
					//on compare les deux noms
					if(temp.getNom().equals(nomJoueur)) {
						j = temp;
					}
				}
			}
			
			//on va lui afficher le joueur
			//jeu.setMessageAAfficher("Voici le joueur = " + j);
			//jeu.setMessageAAfficher("Quelle carte voulez vous prendre ? Tapez v pour visible et nv pour non visible");
		
			//le resulatt
			c.setAttendue(EntreeAttendue.VISIBLE);
			String rep = c.lireVisible();
			
			while(rep.equals("v") == false && rep.equals("nv") == false) {
				//jeu.setMessageAAfficher("Quelle carte voulez vous prendre ? Tapez v pour visible et nv pour non visible");

				//rep = sc.next();
				rep = c.lireVisible();
			}
			
			boolean reponse = false;
			if(rep.equals("v"))
				reponse = true;
			
			//on prend la carte
			Carte acar = j.donnerCarte(reponse);
			jQuiPioche.ajouterJest(acar);
			jQuiPioche.setEstEnCoursDeJeu(false);
			
			return j;
		}else {
			Virtuel v = (Virtuel)jQuiPioche;
			Joueur j = v.prendreCarte(liste);
			
			jQuiPioche.setEstEnCoursDeJeu(false);
			
			return j;
		}
		
	}

}
