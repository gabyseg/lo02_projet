package lo02_projet.controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import lo02_projet.cartes.Carte;
import lo02_projet.joueur.Joueur;

/**
 * Controleur qui permet de déposer une carte
 * @author gabrielsega
 *
 */
public class ControleurDepotCarte {
	/**
	 * Constructeur du controleur
	 * @param bouton bouton au quel il faut ajouter un action listener
	 * @param c carte correspondante
	 * @param j joueur à qui elle apopartient
	 */
	public ControleurDepotCarte(JButton bouton, Carte c, Joueur j) {
		// TODO Auto-generated constructor stub
		bouton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Collecteur col = Collecteur.getInstance();
				col.deposerNomJoueur(j.getNom());
				String ch = "nv";
				if(c.isVisible()) {
					ch = "v";
				}
				col.deposerVisible(ch);
			}
		});
	}
}
