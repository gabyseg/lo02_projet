package lo02_projet.controleur;

/**
 * Ensemble des entrees attendues
 *
 */
public enum EntreeAttendue {
	NOM_JOUEUR, NOMBRE_JOUEURS, VISIBLE, NUMERO_OFFRE, NOM_JOUEUR_PRENDRE_CARTE, STRATEGIE, AJOUT_CARTE, COMPTAGE_POINTS
}
