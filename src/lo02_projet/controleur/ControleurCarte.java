package lo02_projet.controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import lo02_projet.cartes.Carte;

/**
 * Controleur qui sert à retourner une carte
 * @author gabrielsega
 *
 */
public class ControleurCarte {
	
	/**
	 * Constructeur du controleur
	 * @param j Bouton
	 * @param c Carte
	 * @param numDansOffre Numero de la carte (0 ou 1)
	 */
	public ControleurCarte(JButton j, Carte c, int numDansOffre) {
		j.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Collecteur.getInstance().deposerNumeroOffre(numDansOffre+1);
			}
		});
	}
}
