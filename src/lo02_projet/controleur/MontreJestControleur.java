package lo02_projet.controleur;
//
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import lo02_projet.cartes.Carte;
import lo02_projet.joueur.Joueur;

/**
 * 
 * @author gabrielsega
 * Classe permettant d'afficher le jest d'un joueur
 */
public class MontreJestControleur {
	/**
	 * Constructeur 
	 * @param j le bouton sur lequel on ajoute le listener
	 * @param jou le joueur dont nous devons montrer le jest
	 */
	public MontreJestControleur(JButton j, Joueur jou) {
		// TODO Auto-generated constructor stub
		j.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				//la vue ici
				JFrame frame = new JFrame("JEST");
				//fermeture
				//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);				
				
				JPanel jp = new JPanel();
				
				jp.setLayout(new GridBagLayout());
				//on affiche son jest
				for(Carte c : jou.getJest().getCartes()) {
					System.out.println("icicicii");
					Image img = null;
					try {
						img = c.getImage(true);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					img = img.getScaledInstance(img.getWidth(null)/3, img.getHeight(null)/3, Image.SCALE_DEFAULT);
					JLabel label = new JLabel(new ImageIcon(img));
					jp.add(label);
				}
				
				//on affiche		
				frame.add(jp);
				frame.pack();
				jp.requestFocusInWindow();
				frame.setVisible(true);
			}
		});
	}
}
