package lo02_projet.controleur;

/**
 * Classe qui implémente le patron consommateur et vers lequel sont redirigés l'ensemble des éléments
 * @author gabrielsega
 *
 */

public class Collecteur {
	/**
	 * Collecteur
	 */
	private static Collecteur c;
	
	/**
	 * Entree attendue
	 */
	private EntreeAttendue attendue;
	/**
	 * là ou on stocke le nom d'un joueur
	 */
	private String nomJoueur;
	/**
	 * variable qui stocke si c'est visible ou non
	 */
	private String visible;
	/**
	 * Variable qui stocke un nombre
	 */
	private int nombre;
	/**
	 * Variable qui stocke un numéro dans l'offre
	 */
	private int numeroDansOffre;
	/**
	 * variable stockant le numéro d'un stratégie
	 */
	private String strategie;
	/**
	 * variable stockant si l'on ajoute ou non
	 */
	private String ajout;
	/**
	 * variable stockant le type de comptage de points
	 */
	private String comptage;
	
	/**
	 * Constructeur du Collecteur
	 * @param ea Entree attendue
	 */
	private Collecteur(EntreeAttendue ea) {
		this.attendue = ea;
		this.nomJoueur = "";
		this.visible = "";
		this.nombre = -1;
		this.numeroDansOffre = -1;
		this.strategie = "";
		this.ajout = "";
		this.comptage = "";
	}
	
	public static Collecteur getInstance() {
		if(Collecteur.c == null) {
			Collecteur.c = new Collecteur(null);
		}
		return Collecteur.c;
	}
	
	public synchronized void deposerNomJoueur(String m) {
		while(nomJoueur.isEmpty() == false) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(this.attendue == EntreeAttendue.NOM_JOUEUR) {
			this.nomJoueur = m;
		}else if(this.attendue == EntreeAttendue.NOM_JOUEUR_PRENDRE_CARTE) {
			this.nomJoueur = m;
			this.attendue = EntreeAttendue.VISIBLE;
		}
		
		notify();	
	}
	
	public synchronized void deposerStrategie(String m) {
		while(strategie.isEmpty() == false) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(this.attendue == EntreeAttendue.STRATEGIE) {
			this.strategie = m;
		}
		
		notify();	
	}
	
	public synchronized void deposerAjoutCarte(String m) {
		while(ajout.isEmpty() == false) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(this.attendue == EntreeAttendue.AJOUT_CARTE) {
			this.ajout = m;
		}
		
		notify();	
	}
	
	public synchronized void deposerVisible(String v) {
		while(this.visible.isEmpty() == false) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if(this.attendue == EntreeAttendue.VISIBLE) {
			this.visible = v;
		}
		
		notify();	
	}
	
	public synchronized void deposerComptage(String v) {
		while(this.comptage.isEmpty() == false) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if(this.attendue == EntreeAttendue.COMPTAGE_POINTS) {
			this.comptage = v;
		}
		
		notify();	
	}
	
	public synchronized void deposerNbJoueurs(int nb) {
		while(this.nombre != -1) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(this.attendue == EntreeAttendue.NOMBRE_JOUEURS) {
			this.nombre = nb;
		}
		
		notify();	
	}
	
	public synchronized void deposerNumeroOffre(int nb) {
		while(this.numeroDansOffre != -1) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(this.attendue == EntreeAttendue.NUMERO_OFFRE) {
			this.numeroDansOffre = nb;
		}
		
		notify();	
	}

	
	
	public synchronized String lireNomJoueur() {
		while(this.nomJoueur.isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		String ms = this.nomJoueur;
		
		this.nomJoueur = "";
		notify();
		return ms;
	}
	
	public synchronized String lireStrategie() {
		while(this.strategie.isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		String ms = this.strategie;
		
		this.strategie = "";
		
		notify();
		return ms;
	}
	
	public synchronized String lireComptage() {
		while(this.comptage.isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		String ms = this.comptage;
		
		this.comptage = "";
		
		notify();
		return ms;
	}
	
	public synchronized String lireAjoutCarte() {
		while(this.ajout.isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		String ms = this.ajout;
		
		this.ajout = "";
		
		notify();
		return ms;
	}

	public synchronized String lireVisible() {
		while(this.visible.isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		String ms = this.visible;
		
		this.visible = "";
		notify();
		return ms;
	}

	public synchronized int lireNombre() {
		while(this.nombre == -1) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		int nb = this.nombre;
		
		this.nombre = -1;
		notify();
		return nb;
	}
	
	public synchronized int lireNumeroDansOffre() {
		while(this.numeroDansOffre == -1) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		int nb = this.numeroDansOffre;
		
		this.numeroDansOffre = -1;
		notify();
		return nb;
	}
	
	
	
	public EntreeAttendue getAttendue() {
		return attendue;
	}

	public void setAttendue(EntreeAttendue attendue) {
		this.attendue = attendue;
	}

	public String getNomJoueur() {
		return nomJoueur;
	}

	public void setNomJoueur(String nomJoueur) {
		this.nomJoueur = nomJoueur;
	}

	public String getVisible() {
		return visible;
	}

	public void setVisible(String visible) {
		this.visible = visible;
	}

	public int getNombre() {
		return nombre;
	}

	public void setNombre(int nombre) {
		this.nombre = nombre;
	}

	public int getNumeroDansOffre() {
		return numeroDansOffre;
	}

	public void setNumeroDansOffre(int numeroDansOffre) {
		this.numeroDansOffre = numeroDansOffre;
	}

}
