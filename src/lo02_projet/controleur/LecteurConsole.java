package lo02_projet.controleur;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Classe permettant la lecture de la console
 *
 */
public class LecteurConsole implements Runnable{
	
	/**
	 * Reader
	 */
	private BufferedReader br;

	/**
	 * Constructeur du lecteur
	 */
	public LecteurConsole() {
		// TODO Auto-generated constructor stub
		//on instancie le flux
		this.br = new BufferedReader (new InputStreamReader(System.in));

		//on instancie le thread
		Thread t = new Thread(this);
		t.start();
	}

	@Override
	public void run() {
		Collecteur c = Collecteur.getInstance();
		// TODO Auto-generated method stub
		//on lit toujours un string
		String messageLu = "";
		while(true) {
			try {
				//on récupère le message ici
				messageLu = this.br.readLine();

				//ici selon ce qui est attendu on va switch pour affecter à la bonne valeur
				switch (c.getAttendue()) {
				case NOMBRE_JOUEURS:
					try {
						int nb = Integer.parseInt(messageLu);
						c.deposerNbJoueurs(nb);
					}catch (Exception e) {
						// TODO: handle exception
						System.out.println("Recommencez");
					}
					break;
				case NOM_JOUEUR:
					c.deposerNomJoueur(messageLu);
					break;
				case NOM_JOUEUR_PRENDRE_CARTE:
					c.deposerNomJoueur(messageLu);
					break;
				case STRATEGIE:
					if(messageLu.equals("Aléatoire") || messageLu.equals("Invisible") ) {
						c.deposerStrategie(messageLu);
					}
					break;
				case AJOUT_CARTE:
					if(messageLu.equals("Oui") || messageLu.equals("Non") ) {
						c.deposerAjoutCarte(messageLu);
					}
					break;

				case COMPTAGE_POINTS:
					if(messageLu.equals("Classique") || messageLu.equals("Valeur") ) {
						c.deposerComptage(messageLu);
					}
					break;
				case NUMERO_OFFRE:
					try {
						int nb1 = Integer.parseInt(messageLu);
						if(nb1 == 1 || nb1 == 2) {
							c.deposerNumeroOffre(nb1);
						}
					}catch (Exception e) {
						// TODO: handle exception
						System.out.println("Recommencez");
					}
					break;
				case VISIBLE:
					if(messageLu.equals("v") || messageLu.equals("nv") ) {
						c.deposerVisible(messageLu);
					}
					break;
				default:
					break;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
