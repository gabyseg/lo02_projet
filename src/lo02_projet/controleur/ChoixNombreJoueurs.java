package lo02_projet.controleur;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import lo02_projet.jeu.Jeu;

/**
 * 
 * @author gabrielsega
 * Class qui permet d'afficher le panneau pour le choix du nombre de joueurs
 */
public class ChoixNombreJoueurs extends JPanel implements Observer{
	private JLabel labelInstructions;
	private JComboBox<Integer> comboBox;
	private JButton boutonSuivant;
	private boolean choixFait;
	
	/**
	 * Constructeur qui initialise le panneau avec un objet jeu
	 * @param jeu objet jeu à modifier
	 * @throws IOException
	 */
	public ChoixNombreJoueurs(Jeu jeu) throws IOException {
		// TODO Auto-generated constructor stub
		super();
						
		//observer le jeu
		jeu.addObserver(this);
		
		//pour savoir si les choix sont en phase d'être fini
		this.choixFait = false;
		
		//on ajoute le grid layout
		this.setLayout(new GridLayout(3,1));
		
		//le label pour les instructions
		JLabel labelI = new JLabel("Bonjour, bienvenue dans le JEST, combien de joueurs réels voulez vous?");
		labelI.setFont(labelI.getFont().deriveFont(20.0f));
		labelI.setHorizontalAlignment(JLabel.CENTER);
		
		//JCombobox
		Integer[] tableauValeursAutorisees = {1, 2, 3, 4};
		JComboBox<Integer> comboB = new JComboBox<Integer>(tableauValeursAutorisees);
		comboB.setFont(comboB.getFont().deriveFont(16.0f));
		comboB.setPreferredSize(new Dimension(200, 30));
		
		//pour stocker la comboBox
		JPanel j = new JPanel();
		j.add(comboB);
		
		//pour valider et bouton suivant
		JButton jb = new JButton("suivant");
		j.add(jb);
		
		//on ajoute tout
		this.add(labelI);
		this.add(j);

		//on récupère les références
		this.labelInstructions = labelI;
		this.comboBox = comboB;
		this.boutonSuivant = jb;
		
		//action listener du bouton
		ChoixNombreJoueurs jp = this;
		this.boutonSuivant.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				//on va passer la valeur du nombre de joueurs au collecteur
				Collecteur c = Collecteur.getInstance();
				int nbReel = (int)comboBox.getSelectedItem();
				c.deposerNbJoueurs(nbReel);
				
				//on change l'interface pour la suite
				//si on a fini avec les noms des joueurs
				if(choixFait == false) {
					changerInterfaceNbVirtuel(nbReel);
					choixFait = true;
				}else {
					changerInterfaceNoms(jeu);
					try {
						jeu.deleteObserver(jp);
						changerInterfaceNoms(jeu);
					}catch (Exception ex) {
						// TODO: handle exception
						
					}
				}
				
			}
		});
	}
	
	@Override
	public void repaint() {
		// TODO Auto-generated method stub
	}
	
	
	/**
	 * Méthode qui permet de changer l'interface, au lieu de choisir le nombre de joueurs réels on choisi le nombre de joueurs virtuels
	 * @param nbReel
	 */
	public void changerInterfaceNbVirtuel(int nbReel) {
		//on vient regarder combien au maximum ils peuvent prendre 
		int nbPossiblesRestants = 4-nbReel;
		int minimum = 3-nbReel;
		
		//if(nbReel==1)
			//nbPossiblesRestants++;
		
		//on créé la nouvelle liste de valeur
		ArrayList<Integer> t = new ArrayList<Integer>();
		for(int i=minimum; i<=nbPossiblesRestants; i++) {
			t.add(i);
		}
		
		this.comboBox.setModel(new DefaultComboBoxModel<Integer>(t.toArray(new Integer[0])));
		this.labelInstructions.setText("Combien de joueurs virtuels voulez vous?");
	}
	
	/**
	 * Méthode qui permet d'afficher le panneau du choix des noms
	 * @param j l'instance de jeu en cours
	 */
	public void changerInterfaceNoms(Jeu j) {
		//on récupère la fenêtre
		JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
		//on efface tout
		topFrame.getContentPane().removeAll();
		//on créé la partie qui va nous permettre d'ajouter les noms
		ControleurNoms cn = new ControleurNoms(j);
		topFrame.getContentPane().add(cn);
		//on rafraichit
		//SwingUtilities.updateComponentTreeUI(topFrame);
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		if(o instanceof Jeu) {
			Jeu j = (Jeu)o;
			
			//si le jeu n'a pas de joueur virtuels on passe à la suivant
			if(j.getNbVirtuel() == -1) {
				this.choixFait = true;
				this.changerInterfaceNbVirtuel(j.getNbReels());
			}else {
				try {
					j.deleteObserver(this);
					this.changerInterfaceNoms(j);
				}catch (Exception e) {
					// TODO: handle exception
					
				}
			}
		}
	}
}
