package lo02_projet.joueur;

import java.util.ArrayList;
import java.util.Random;

import lo02_projet.cartes.Carreau;
import lo02_projet.cartes.Carte;
import lo02_projet.cartes.Pique;
import lo02_projet.strategies.Aleatoire;
import lo02_projet.strategies.StrategieBots;

/**
 * Classe representant un joueur virtuel
 *
 */
public class Virtuel extends Joueur {
	
	/**
	 * Strategie du joueur virtuel
	 */
	private StrategieBots strategie;

	/**
	 * Constructeur du joueur virtuel
	 * @param nom Nom du joueur
	 * @param s Strategie du joueur
	 */
	public Virtuel(String nom, StrategieBots s) {
		super(nom);
		this.strategie = s;
	}

	/**
	 * Effectuer une offre, appel a la strategie du joueur virtuel pour definir la carte a rendre visible
	 */
	public void faireOffre() {
		// TODO Auto-generated method stub
		// Changement de la visibilité de la carte choisie
		Carte[] offre = this.getOffre();
		int choix = this.strategie.faireOffre(offre);
		
		offre[choix].setVisible(true);
		setChanged();
		notifyObservers();
	}

	/**
	 * Methode permettant d'effectuer le choix de la carte a recuperer, appel a la strategie du joueur virtuel
	 * @param lj Liste des joueurs
	 * @return Retourne le joueur qui a ete choisi
	 * @throws Exception
	 */
	public Joueur prendreCarte(ArrayList<Joueur> lj) throws Exception {
		// TODO Auto-generated method stub
		ArrayList<Joueur> liste = new ArrayList<Joueur>(lj);
		//on récupère d'abord le joueur
		if(liste.size() != 1)
			liste.remove(this);
		
		Joueur j = this.strategie.choisirJoueur(liste);
		//on récupère la carte
		Carte c = this.strategie.choisirCarte(j);
		this.ajouterJest(c);
		
		setChanged();
		notifyObservers();
		
		return j;
	}

	/**
	 * Indique si le joueur est reel
	 */
	public boolean estReel() {
		// TODO Auto-generated method stub
		return false;
	}

}
