package lo02_projet.joueur;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Scanner;

import lo02_projet.cartes.Carreau;
import lo02_projet.cartes.Carte;
import lo02_projet.cartes.Pique;

/**
 * Classe representant un joueur reel
 *
 */
public class Reel extends Joueur {

	/**
	 * Constructeur du joueur reel
	 * @param nom Nom du joueur
	 */
	public Reel(String nom) {
		super(nom);
	}
	
	/**
	 * Methode toString
	 * @return Retourne le nom du joueur et son offre
	 */
	public String toString() {
		String res = "";
		
		res += super.toString();
		
		return res;
	}

	/**
	 * Indique si le joueur est reel
	 */
	public boolean estReel() {
		// TODO Auto-generated method stub
		return true;
	}
	

}
