package lo02_projet.joueur;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Scanner;

import lo02_projet.cartes.Carreau;
import lo02_projet.cartes.Carte;
import lo02_projet.cartes.Coeur;
import lo02_projet.cartes.Condition;
import lo02_projet.cartes.Couleur;
import lo02_projet.cartes.Jest;
import lo02_projet.cartes.Pique;
import lo02_projet.cartes.Trefle;

/**
 * Classe representant un joueur
 *
 */
public abstract class Joueur extends Observable{
	
	/**
	 * Jest du joueur
	 */
	private Jest jest;
	
	/**
	 * Nom du joueur
	 */
	private String nom;
	
	/**
	 * Offre du joueur (contient les deux cartes qu'il presente à chaque tour)
	 */
	private Carte[] offre;
	
	/**
	 * Indique si le joueur a deja joue
	 */
	private boolean aDejaJoue;
	
	/**
	 * Indique si le joueur est en train de jouer
	 */
	private boolean estEnCoursDeJeu;
	
;	
	/**
	 * Constructeur du joueur
	 * @param nom Nom du joueur
	 */
	public Joueur(String nom) {
		this.jest = new Jest();
		this.nom = nom;
		this.offre = new Carte[2];
		this.aDejaJoue = false;
		this.estEnCoursDeJeu = false;
	}

	/**
	 * Ajoute une carte au Jest du joueur
	 * @param c Carte a ajouter
	 */
	public void ajouterJest(Carte c) {
		this.jest.ajouterCarte(c);
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Ajoute une carte a l'offre du joueur
	 * @param c Carte a ajouter
	 */
	public void ajouterCarteOffre(Carte c) {
		if (offre[0] == null) {
			offre[0] = c;
			offre[0].setVisible(false);
		} else {
			offre[1] = c;
			offre[1].setVisible(false);
		}
		setChanged();
		notifyObservers();
	}
		
	/**
	 * Recupere l'offre du joueur
	 * @return Retourne une liste de cartes
	 */
	public Carte[] recupererOffre() {
		return offre;
	}
	
	/**
	 * Prend la derniere carte restante
	 */
	public void prendreDerniereCarte() {
		for(Carte c : this.offre) {
			if(c != null)
				this.ajouterJest(c);
		}
		this.effacerOffre();
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Efface l'offre du joueur
	 */
	public void effacerOffre() {
		for(int i =0; i<this.offre.length; i++) {
			this.offre[i] = null;
		}
		setChanged();
		notifyObservers();
	}
		
	/**
	 * Recupere la carte visible du joueur
	 * @return Retourne une carte
	 */
	public Carte getCarteRetournee() {
		if (offre[0].isVisible()) {
			return offre[0];
		} else {
			return offre[1];
		}
	}
	
	/**
	 * Recupere la carte ayant la plus forte valeur
	 * @param nb Valeur de reference
	 * @return Retourne une carte
	 */
	public Carte meilleureCarteValeur(int nb) {
		Carte c = null;
		
		ArrayList<Carte> cartes = new ArrayList<Carte>(this.jest.getCartes());
		for(Carte carte: cartes) {
			if(carte.getValeur() == nb) {
				if(c == null) {
					c = carte;
				}else if(c.getValeur() == nb){
					if(c.getCouleur().compareTo(carte.getCouleur())<0) {
						c = carte;
					}
				}
			}
		}
		return c;
	}
	
	/**
	 * Donne une carte de l'offre
	 * @param visible Choix de la carte, visible ou non
	 * @return Retourne la carte choisie
	 * @throws Exception
	 */
	public Carte donnerCarte(Boolean visible) throws Exception {
		Carte c = null;
		
		//on vérifie que l'on a déjà des cartes
		if(this.offre[0] != null && this.offre[0].isVisible() == visible) {
			c = this.offre[0];
			this.offre[0] = null;
		}else if(this.offre[1] != null && this.offre[1].isVisible() == visible){
			c = this.offre[1];
			this.offre[1] = null;
		}else {
			throw new Exception("Problème de sélection de carte l'offre, demandé : " + visible +"/" + 
							"Carte 0 : " +
							this.offre[0].isVisible()+
							"Carte 1: " +
							this.offre[1].isVisible()+ "/////"+
							this.toString());
		}
		setChanged();
		notifyObservers();
		return c;
	}
	
	/**
	 * Indique le nombre de cartes restantes dans l'offre du joueur
	 * @return Retourne le nombre de cartes
	 */
	public int nombreCartesRestantesOffre() {
		int nb =0;
		for(int i=0; i<this.offre.length; i++) {
			if(this.offre[i] != null)
				nb++;
		}
		return nb;
	}
	
	/**
	 * Methode toString
	 * @return Retourne le nom du joueur et son offre
	 */
	public String toString() {
		String res = "\nJoueur : " + this.nom +", ";
		
		res += "\nOffre : ";
		for(int i =0; i<this.offre.length; i++) {
			res += this.offre[i] + ", ";
		}
		
		res += "\nA deja joué : " + this.aDejaJoue + ", ";
		res += "\nJest : " + this.jest + "\n";
		
		return res;
	}
	
	/**
	 * Recupere le Jest du joueur
	 * @return Jest
	 */
	public Jest getJest() {
		return jest;
	}
	
	/**
	 * Change le Jest du joueur
	 * @param jest Nouveau Jest
	 */
	public void setJest(Jest jest) {
		this.jest = jest;
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Recupere le nom du joueur
	 * @return Nom du joueur
	 */
	public String getNom() {
		return nom;
	}
	
	/**
	 * Change le nom du joueur
	 * @param nom Nouveau nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
		setChanged();
		notifyObservers();
	}

	/**
	 * Recupere l'offre du joueur
	 * @return Offre du joueur
	 */
	public Carte[] getOffre() {
		return offre;
	}

	/**
	 * Change l'offre du joueur
	 * @param offre Nouvelle offre
	 */
	public void setOffre(Carte[] offre) {
		this.offre = offre;
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Indique si le joueur a deja joue
	 * @return Retourne true ou false
	 */
	public boolean isaDejaJoue() {
		return aDejaJoue;
	}
	
	/**
	 * Change si le joueur a deja joue
	 * @param aDejaJoue Indique si le joueur a deja joue
	 */
	public void setaDejaJoue(boolean aDejaJoue) {
		this.aDejaJoue = aDejaJoue;
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Indique si le joueur est reel
	 * @return Retourne true ou false
	 */
	public abstract boolean estReel();

	/**
	 * Indique si le joueur est en cours de jeu
	 * @return Retourne true ou false
	 */
	public boolean isEstEnCoursDeJeu() {
		return estEnCoursDeJeu;
	}

	/**
	 * Change si le joueur est en cours de jeu
	 * @param estEnCoursDeJeu Indique si le joueur est en cours de jeu
	 */
	public void setEstEnCoursDeJeu(boolean estEnCoursDeJeu) {
		this.estEnCoursDeJeu = estEnCoursDeJeu;
		setChanged();
		notifyObservers();
	}
	
	
	
}
