package lo02_projet.jeu;

import lo02_projet.cartes.Carreau;
import lo02_projet.cartes.Carte;
import lo02_projet.cartes.Coeur;
import lo02_projet.cartes.Jest;
import lo02_projet.cartes.Joker;
import lo02_projet.cartes.Mystere;
import lo02_projet.cartes.Pique;
import lo02_projet.cartes.Trefle;
import lo02_visiteur.Visiteur;

/**
 * Classe correspondant a un compteur de points
 *
 */
public class CompteurDePoints implements Visiteur{
	
	/**
	 * Nombre de coeurs
	 */
	private int nbCoeurs;
	
	/**
	 * Presence d'un joker
	 */
	private boolean joker;
	
	/**
	 * Recupere le nombre de points associes au coeur
	 */
	public int visiter(Coeur c) {
		int nbPoints = 0;
		
		// TODO Auto-generated method stub
		if(joker && this.nbCoeurs >= 1 && this.nbCoeurs<=3) {
			nbPoints = -c.getValeurReelle();
		}else if(this.nbCoeurs == 4) {
			nbPoints = c.getValeurReelle();
		}
		
		return nbPoints;
	}

	/**
	 * Recupere le nombre de points associes au carreau
	 */
	public int visiter(Carreau c) {
		// TODO Auto-generated method stub
		return -c.getValeurReelle();
	}

	/**
	 * Recupere le nombre de points associes au pique
	 */
	public int visiter(Pique p) {
		// TODO Auto-generated method stub
		return p.getValeurReelle();
	}

	/**
	 * Recupere le nombre de points associes au trefle
	 */
	public int visiter(Trefle c) {
		// TODO Auto-generated method stub
		return c.getValeurReelle();
	}

	/**
	 * Recupere le nombre de points associes au joker
	 */
	public int visiter(Joker c) {
		// TODO Auto-generated method stub
		return 0;
	}



	/**
	 * Calcul des points
	 */
	public int visiter(Jest j) {
		// TODO Auto-generated method stub
		this.nbCoeurs = j.nombreCoeurs();
		this.joker = j.aUnJoker();
		return this.compterPoints(j);
	}
	
	/**
	 * Compteur de points d'un Jest
	 * @param j Jest
	 * @return Retourne un nombre de points
	 */
	public int compterPoints(Jest j) {
		int points = 0;
		/*
		 * si il a un joker
		 * 		si il n'a pas de coeurs
		 * 		sinon si il en a moins de 3
		 * 		sinon si il les as tous
		 * sinon
		 * 	on compte bêtement
		 */
		if(j.aUnJoker()) {
			//si il n'a pas de coeurs
			if(j.nombreCoeurs() == 0) {
				//on rajoute le bonus de 4 points
				points += 4;
			}
			//Sinon on parcourt les cartes
			for(Carte c : j.getCartes()) {
				points += c.accepter(this);
			}
		}else {
			for(Carte c : j.getCartes()) {
				points += c.accepter(this);
			}
		}
		
		//on ajoute les points des paires noires
		points += this.augmentationPointsViaPaires(j);
		
		return points;
	}
	
	/**
	 * Compte le nombre de paires noires
	 * @param j Jest
	 * @return Retourne un nombre
	 */
	public int augmentationPointsViaPaires(Jest j) {
		int nbPairesNoires = 0;
		
		//pour chaque carte
		for(Carte c : j.getCartes()) {
			//si c'est une carte noire on cherche sa paire
			if(c.estCarteNoire()) {
				
				//pour chaque carte
				for(Carte c2 : j.getCartes()) {
					//si c'est une carte noire et qu'elle a la même valeur et qu'elle est différente de c
					if(c2.estCarteNoire() && c2.getValeur()==c.getValeur() && c != c2) {
						nbPairesNoires++;
					}
				}
				
			}
		}
		
		
		return nbPairesNoires;
	}

	/**
	 * Recupere le nombre de points associes a la carte mystere
	 */
	public int visiter(Mystere mystere) {
		// TODO Auto-generated method stub
		int valeur = -1;
		switch(mystere.getCouleur()) {
		case CARREAU:
			valeur = this.visiter(new Carreau(mystere.getValeur(), null, null));
			break;
		case COEUR:
			valeur = this.visiter(new Coeur(mystere.getValeur(), null, null));
			break;
		case PIQUE:
			valeur = this.visiter(new Pique(mystere.getValeur(), null, null));
			break;
		case TREFLE:
			valeur = this.visiter(new Trefle(mystere.getValeur(), null, null));
			break;
		default:
			valeur = 0;
			break;
		
		}
		return valeur;
	}
}
