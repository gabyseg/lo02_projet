package lo02_projet.jeu;

import lo02_projet.cartes.Carreau;
import lo02_projet.cartes.Carte;
import lo02_projet.cartes.Coeur;
import lo02_projet.cartes.Jest;
import lo02_projet.cartes.Joker;
import lo02_projet.cartes.Mystere;
import lo02_projet.cartes.Pique;
import lo02_projet.cartes.Trefle;
import lo02_visiteur.Visite;
import lo02_visiteur.Visiteur;

/**
 * Classe correspondant a un compteur de points plus simple
 *
 */
public class CompteurSimple implements Visiteur{

	/**
	 * Recupere le nombre de points associes au coeur
	 */
	public int visiter(Coeur c) {
		// TODO Auto-generated method stub
		return c.getValeurReelle();
	}

	/**
	 * Recupere le nombre de points associes au carreau
	 */
	public int visiter(Carreau c) {
		// TODO Auto-generated method stub
		return c.getValeurReelle();
	}

	/**
	 * Recupere le nombre de points associes au pique
	 */
	public int visiter(Pique p) {
		// TODO Auto-generated method stub
		return p.getValeurReelle();
	}

	/**
	 * Recupere le nombre de points associes au trefle
	 */
	public int visiter(Trefle t) {
		// TODO Auto-generated method stub
		return t.getValeurReelle();
	}

	/**
	 * Recupere le nombre de points associes au joker
	 */
	public int visiter(Joker j) {
		// TODO Auto-generated method stub
		return j.getValeurReelle();
	}

	/**
	 * Calcul des points
	 */
	public int visiter(Jest j) {
		// TODO Auto-generated method stub
		int points = 0;

		//on additionne tous les points
		for(Carte c : j.getCartes()) {
			points += c.accepter(this);
		}

		//on ajoute les points des paires noires
		points += this.augmentationPointsViaPaires(j);

		return points;
	}
	
	/**
	 * Compte le nombre de paires noires
	 * @param j Jest
	 * @return Retourne un nombre
	 */
	public int augmentationPointsViaPaires(Jest j) {
		int nbPairesNoires = 0;
		
		//pour chaque carte
		for(Carte c : j.getCartes()) {
			//si c'est une carte noire on cherche sa paire
			if(c.estCarteNoire()) {
				
				//pour chaque carte
				for(Carte c2 : j.getCartes()) {
					//si c'est une carte noire et qu'elle a la même valeur et qu'elle est différente de c
					if(c2.estCarteNoire() && c2.getValeur()==c.getValeur() && c != c2) {
						nbPairesNoires++;
					}
				}
				
			}
		}
		
		
		return nbPairesNoires;
	}

	/**
	 * Recupere le nombre de points associes a la carte mystere
	 */
	public int visiter(Mystere mystere) {
		// TODO Auto-generated method stub
		return mystere.getValeurReelle();
	}



}
