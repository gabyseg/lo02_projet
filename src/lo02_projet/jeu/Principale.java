package lo02_projet.jeu;

import java.awt.Dimension;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import lo02_projet.controleur.ChoixNombreJoueurs;
import lo02_projet.controleur.Collecteur;
import lo02_projet.controleur.ControleurJeu;
import lo02_projet.controleur.ControleurNoms;
import lo02_projet.controleur.EntreeAttendue;
import lo02_projet.controleur.LecteurConsole;
import lo02_projet.joueur.Joueur;
import lo02_projet.joueur.Reel;
import lo02_projet.joueur.Virtuel;
import lo02_projet.strategies.Aleatoire;
import lo02_projet.strategies.Gauche;
import lo02_projet.strategies.StrategieBots;
import lo02_projet.vue.VueJeu;
import lo02_projet.vue.VueTexte;

/**
 * Classe de lancement du jeu
 *
 */
public class Principale {

	public static void main(String[] args) throws Exception {
		//on initialise le jeu
		Jeu j = new Jeu();

		//on instancie le collecteur ici
		//Collecteur c =  new Collecteur(null);
		Collecteur c = Collecteur.getInstance();
		c.setAttendue(EntreeAttendue.NOMBRE_JOUEURS);
		LecteurConsole l = new LecteurConsole();
		//la vue ici
		JFrame frame = new JFrame("JEST");
		//fermeture
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//on instancie la vue de départ
		ChoixNombreJoueurs cnj = new ChoixNombreJoueurs(j);
		cnj.setPreferredSize(new Dimension(1200,700));
		//on affiche		
		frame.add(cnj);
		frame.pack();
		cnj.requestFocusInWindow();
		frame.setVisible(true);


		//on demande le nombre de joueurs
		int nbreel = 0;
		while(nbreel<1 || nbreel>4) {
			System.out.println("Combien de joueurs réels va-t-il y avoir? Attention il ne peut y avoir que 4 joueurs au maximum");
			nbreel = c.lireNombre();
		}
		j.setNbReels(nbreel);

		int nbVirtuel = -1;
		while(nbVirtuel<0 || nbVirtuel>4 || nbVirtuel+nbreel>4) {
			System.out.println("Combien de joueurs virtuels va-t-il y avoir? Attention il ne peut y avoir que 4 joueurs au maximum");
			nbVirtuel = c.lireNombre();
		}
		j.setNbVirtuel(nbVirtuel);

		ControleurNoms cn = new ControleurNoms(j);
		frame.getContentPane().add(cn);
		frame.revalidate();
		frame.repaint();


		//ici on va lire la strategie
		//on met le lecteur en mode String
		c.setAttendue(EntreeAttendue.STRATEGIE);
		System.out.println("Entrez une stratégie pour le joueur : Aléatoire ou Invisible");
		String strat = c.lireStrategie();
		StrategieBots stb = null;
		if(strat.equals("Aléatoire")) {
			stb = new Aleatoire();
		}else if(strat.equals("Invisible")) {
			stb = new Gauche();
		}

		//dès qu'on a l'aléatoire on bloque la vue
		cn.desactiverDifficulte();

		//ici on fait pour l'ajout d'une carte
		c.setAttendue(EntreeAttendue.AJOUT_CARTE);
		System.out.println("Voulez vous ajouter une carte mystère : vous ne connaissez ni la couleur ni la valeur de cette carte?");
		System.out.println("Tapez Oui ou Non");
		String ajout = c.lireAjoutCarte();
		boolean ajt = true;
		if(ajout.equals("Non")) {
			ajt = false;
		}
		//on désactive la case d'ajout
		cn.desactiverAjout(ajt);

		//on rajoute les cartes mystère
		j.ajouterCarteMystere(ajt, nbreel+nbVirtuel);

		//pour le comptage des points
		c.setAttendue(EntreeAttendue.COMPTAGE_POINTS);
		System.out.println("Comment voulez vous compter les points?");
		System.out.println("Tapez Classique pour utiliser les règles classiques, ou Valeur pour utiliser uniquement la valeur des cartes");
		String val = c.lireComptage();
		if(val.equals("Valeur")) {
			j.setCompteur(new CompteurSimple());
		}

		cn.desactiverCompteur();


		//on met le lecteur en mode String
		c.setAttendue(EntreeAttendue.NOM_JOUEUR);

		//on créé les joueurs
		ArrayList<Joueur> liste = new ArrayList<Joueur>();
		for(int i=0; i<nbreel; i++) {
			System.out.println("Rentrez le nom du " + (i+1) + "e joueur réel");
			String nom = c.lireNomJoueur();
			j.ajouterJoueur(new Reel(nom));
		}

		//les virtuels
		for(int i=0; i<nbVirtuel; i++) {
			System.out.println("Rentrez le nom du " + (i+1) + "e joueur virtuel");
			String nom = c.lireNomJoueur();
			j.ajouterJoueur(new Virtuel(nom, stb));
		}

		//arrivé ici on remet le nouveau panel du jeu
		VueJeu vj;
		frame.getContentPane().removeAll();
		try {
			vj = new VueJeu(j);
			frame.getContentPane().add(vj);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		//on instancie le controleur du jeu
		ControleurJeu cj = new ControleurJeu(j);
		VueTexte v = new VueTexte(j);

		cj.initialiserjeu();
		cj.effectuerPartie();
	}

}
