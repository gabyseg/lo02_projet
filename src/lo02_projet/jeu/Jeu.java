package lo02_projet.jeu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Observable;
import lo02_projet.cartes.Carte;
import lo02_projet.cartes.Couleur;
import lo02_projet.cartes.Pioche;
import lo02_projet.joueur.Joueur;
import lo02_visiteur.Visiteur;

/**
 * Classe representant les differentes actions du jeu
 *
 */
public class Jeu extends Observable{
	
	/**
	 * Nombre de joueur(s) reel(s)
	 */
	private int nbReels;
	
	/**
	 * Nombre de joueur(s) virtuel(s) (bot)
	 */
	private int nbVirtuel;
	
	/**
	 * Pioche du jeu
	 */
	private Pioche pioche;
	
	/**
	 * Trophes du jeu
	 */
	private ArrayList<Carte> trophees;
	
	/**
	 * Cartes restantes a la fin de chaque tour
	 */
	private LinkedList<Carte> cartesRestantes;
	
	/**
	 * Liste des joueurs
	 */
	private ArrayList<Joueur> joueurs;
	
	/**
	 * Compteur de points
	 */
	private Visiteur compteur;
	
	//private String messageAAfficher;
	
	/**
	 * Joueur en train de jouer
	 */
	private Joueur joueurEnCours;
	
	/**
	 * Indicateur de fin de jeu
	 */
	private boolean estFini;
	
	/**
	 * Constructeur du jeu
	 */
	public Jeu() {
		//messageAAfficher = "";
		
		//on créé la pioche
		this.pioche = new Pioche();
		this.pioche.melanger();
		//on créé les listes
		this.trophees = new ArrayList<Carte>();
		this.cartesRestantes = new LinkedList<Carte>();
		//on ajoute les joueurs
		this.joueurs = new ArrayList<Joueur>();
		
		//le compteur de points
		this.compteur = new CompteurDePoints();
		
		this.joueurEnCours = null;
		
		this.nbVirtuel = -1;
		
		this.estFini = false;
	}
	
	/**
	 * Recupere les scores des differents joueurs
	 * @return Retourne une chaine de caracteres contenant les noms des joueurs et leur nombre de points
	 */
	public String getScores() {
		String ch = "";
		for(Joueur j : this.joueurs) {
			ch += j.getNom() + " a : " + j.getJest().accepter(this.compteur) +" de points \n";
		}
		return ch;
	}
	
	/**
	 * Distribue un nombre donne de cartes a chaque joueur
	 * @param nb Nombre de cartes a distribuer
	 */
	public void distribuer(int nb) {
		Collections.shuffle(this.cartesRestantes);
		for(int i=0; i<nb; i++) {
			for(Joueur j : this.joueurs) {
				j.ajouterCarteOffre(this.cartesRestantes.pop());
			}
		}
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Complete les cartes restantes avec la pioche afin de pouvoir redistribuer des cartes aux joueurs
	 */
	public void completer() {
		this.pioche.melanger();
		int nbATirer = (this.nbReels + this.nbVirtuel)*2 - this.cartesRestantes.size();
		//this.cartesRestantes.addAll(this.pioche.tirer(this.nbReels + this.nbVirtuel));
		this.cartesRestantes.addAll(this.pioche.tirer(nbATirer));
		//setChanged();
		//notifyObservers();
	}
	
	/**
	 * Recupere les offres restantes des joueurs a la fin du tour
	 */
	public void recupererOffres() {
		//on récupère les offres restantes de tous les joueurs
		for(Joueur j : this.joueurs) {
			//on met les cartes
			LinkedList<Carte> l = new LinkedList<Carte>();
			
			for(Carte c : j.recupererOffre()) {
				if(c != null)
					l.add(c);
			}
			
			//on les ajoutes directement dans les cartes qui restent
			this.cartesRestantes.addAll(l);
			j.effacerOffre();
		}
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Defini le premier joueur a commencer a l'aide des cartes visibles
	 * @return Retourne un joueur
	 */
	public Joueur definirPremierJoueur() {
		Joueur j = null;
		
		//on parcourt tous les joueurs
		for(Joueur jou : this.joueurs) {
			if(j!= null && j.getCarteRetournee().getValeur()<jou.getCarteRetournee().getValeur()) {
				j = jou;
			}else if(j!= null && j.getCarteRetournee().getValeur()==jou.getCarteRetournee().getValeur()) {
				//on compare donc leur couleur
				if(j.getCarteRetournee().getCouleur().compareTo(jou.getCarteRetournee().getCouleur())<0) {
					j = jou;
				}
			}else if(j == null){
				j = jou;
			}
		}
		
		return j;
	}
	
	/**
	 * Recupere le joueur en train de jouer
	 * @return Retourne un joueur
	 */
	public Joueur getJoueurEnCours() {
		return this.joueurEnCours;
	}
	
	/**
	 * Defini le prochain joueur qui n'a pas encore joue et qui possede la carte la plus forte
	 * @return Retourne un joueur
	 */
	public Joueur definirPremierJoueurPasDejaJoue() {
		Joueur j = null;
		
		//on parcourt tous les joueurs
		for(Joueur jou : this.joueurs) {
			if(jou.isaDejaJoue() == false) {
				if(j!= null && j.getCarteRetournee().getValeur()<jou.getCarteRetournee().getValeur()) {
					j = jou;
				}else if(j!= null && j.getCarteRetournee().getValeur()==jou.getCarteRetournee().getValeur()) {
					//on compare donc leur couleur
					if(j.getCarteRetournee().getCouleur().compareTo(jou.getCarteRetournee().getCouleur())<0) {
						j = jou;
					}
				}else if(j == null){
					j = jou;
				}
			}
		}
		
		return j;
	}

	/**
	 * Reinitialise les joueurs en remettant aDejaJoue a false
	 */
	public void reinitialiserJoueurs() {
		//on parcourt la liste
		for(Joueur j : this.joueurs) {
			j.setaDejaJoue(false);
		}
		
		this.joueurEnCours = null;
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Recupere le nombre de joueurs qui n'ont pas encore joue
	 * @return Retourne un nombre
	 */
	public int ontPasJoue() {
		//on parcourt la liste
		int res = 0;
		for(Joueur j : this.joueurs) {
			if(j.isaDejaJoue() == false)
				res++;
		}
		
		return res;
	}
	
	/**
	 * Recupere les joueurs a qui l'on peut prendre une carte
	 * @return Retourne une liste de joueurs
	 */
	public ArrayList<Joueur> getJoueursPiochables(){
		ArrayList<Joueur> j = new ArrayList<Joueur>();
		
		for(Joueur jou : this.joueurs) {
			if(jou.nombreCartesRestantesOffre() == 2) {
				j.add(jou);
			}
		}
		
		return j;
	}
	
	/**
	 * Verifie si le jeu est termine
	 */
	public void estFini() {
		boolean fini = false;
		
		//on vérifie que les joueurs n'ont plus rien dans leur offre
		int nbPersOntOffre = 0;
		for(Joueur j : this.joueurs) {
			if(j.nombreCartesRestantesOffre() != 0) {
				nbPersOntOffre++;
			}
		}
		
		if(this.pioche.getCartes().size() == 0 && this.cartesRestantes.size() == 0 && nbPersOntOffre==0) {
			fini = true;
			setChanged();
			notifyObservers();
		}
		
		this.estFini = fini;
	}
	
	/**
	 * Indique si le jeu est termine
	 * @return Retourne true ou false
	 */
	public boolean getEstFini() {
		return this.estFini;
	}
	
	/**
	 * Distribue les trophes aux joueurs les ayant remportes
	 */
	public void attribuerTrophees() {
		//pour chaque trophé
		for(Carte c : this.trophees) {
			switch (c.getCondition()) {
			case MEILLEUR_JEST:
				Joueur jouMeilleur = null;
				int valeur = 0;
				
				//on parcourt tous les joueurs
				for(Joueur j : this.joueurs) {
					if(jouMeilleur == null) {
						jouMeilleur = j;
						valeur = j.getJest().accepter(this.compteur);
					}else {
						if(valeur < j.getJest().accepter(this.compteur)) {
							jouMeilleur = j;
							valeur = j.getJest().accepter(this.compteur);
						}
					}
				}
				
				jouMeilleur.ajouterJest(c);
				break;
			case JOKER:
				Joueur joueur = null;
				for(Joueur j : this.joueurs) {
					//à revoir
					for(Carte carte : j.getJest().getCartes()) {
						if(carte.estJoker()) {
							joueur = j;
						}
					}
				}
				joueur.ajouterJest(c);
				break;
			case PLUS_PETITE_TREFLE:
				Carte plusPetite = null;
				Joueur joueur1 = null;
				
				for(Joueur j : this.joueurs) {
					for(Carte carte : j.getJest().getCartes()) {
						if(carte.getCouleur() == Couleur.TREFLE) {
							if(plusPetite == null) {
								plusPetite = carte;
								joueur1 = j;
							}else if(plusPetite.getValeurReelle()>carte.getValeurReelle()) {
								plusPetite = carte;
								joueur1 = j;
							}
						}
					}
				}
				
				joueur1.ajouterJest(c);
				break;
			case PLUS_PETITE_PIQUE:
				Carte plusPetite1 = null;
				Joueur joueur11 = null;
				
				for(Joueur j : this.joueurs) {
					for(Carte carte : j.getJest().getCartes()) {
						if(carte.getCouleur() == Couleur.PIQUE) {
							if(plusPetite1 == null) {
								plusPetite1 = carte;
								joueur11 = j;
							}else if(plusPetite1.getValeurReelle()>carte.getValeurReelle()) {
								plusPetite1 = carte;
								joueur11 = j;
							}
						}
					}
				}
				
				joueur11.ajouterJest(c);
				break;
			case MEILLEUR_JEST_SANS_JOKER:
				jouMeilleur = null;
				valeur = 0;
				
				//on parcourt tous les joueurs
				for(Joueur j : this.joueurs) {
					if(jouMeilleur == null || jouMeilleur.getJest().aUnJoker() == false) {
						jouMeilleur = j;
						valeur = j.getJest().accepter(this.compteur);
					}else {
						if(valeur < j.getJest().accepter(this.compteur) && jouMeilleur.getJest().aUnJoker() == false) {
							jouMeilleur = j;
							valeur = j.getJest().accepter(this.compteur);
						}
					}
				}
				
				jouMeilleur.ajouterJest(c);
				
				break;
			case PLUS_FORT_TREFLE:
				Carte plusFort = null;
				joueur11 = null;
				
				for(Joueur j : this.joueurs) {
					for(Carte carte : j.getJest().getCartes()) {
						if(carte.getCouleur() == Couleur.TREFLE) {
							if(plusFort == null) {
								plusPetite1 = carte;
								joueur11 = j;
							}else if(plusFort.getValeurReelle()<carte.getValeurReelle()) {
								plusPetite1 = carte;
								joueur11 = j;
							}
						}
					}
				}
				
				joueur11.ajouterJest(c);
				break;
			case PLUS_FORT_PIQUE:
				plusFort = null;
				joueur11 = null;
				
				for(Joueur j : this.joueurs) {
					for(Carte carte : j.getJest().getCartes()) {
						if(carte.getCouleur() == Couleur.PIQUE) {
							if(plusFort == null) {
								plusPetite1 = carte;
								joueur11 = j;
							}else if(plusFort.getValeurReelle()<carte.getValeurReelle()) {
								plusPetite1 = carte;
								joueur11 = j;
							}
						}
					}
				}
				
				joueur11.ajouterJest(c);
				break;
			case JOUEUR_MAX_4:
				int nbMax = 0;
				joueur11 = null;
				
				for(Joueur j : this.joueurs) {
					int sonMax = 0;
					for(Carte carte : j.getJest().getCartes()) {
						if(carte.getValeur() == 4) {
							sonMax++;
						}
					}
					if(sonMax>nbMax) {
						nbMax = sonMax;
						joueur11 = j;
					}else if(sonMax == nbMax) {
						//alors on va comparer leur meilleure carte de valeur 4
						if(joueur11.meilleureCarteValeur(4).getCouleur().compareTo(j.meilleureCarteValeur(4).getCouleur())<0 && joueur11 != null && j.meilleureCarteValeur(4) != null && joueur11.meilleureCarteValeur(4) != null && joueur11 != null && j != null && joueur11.meilleureCarteValeur(4).getCouleur() != null && j.meilleureCarteValeur(4).getCouleur() != null) {
							joueur11 = j;
						}
					}
				}
				
				joueur11.ajouterJest(c);
				break;
			case JOUEUR_MAX_3:
				nbMax = 0;
				joueur11 = null;
				
				for(Joueur j : this.joueurs) {
					int sonMax = 0;
					for(Carte carte : j.getJest().getCartes()) {
						if(carte.getValeur() == 3) {
							sonMax++;
						}
					}
					if(sonMax>nbMax) {
						nbMax = sonMax;
						joueur11 = j;
					}else if(sonMax == nbMax) {
						//alors on va comparer leur meilleure carte de valeur 3
						if(joueur11.meilleureCarteValeur(3).getCouleur().compareTo(j.meilleureCarteValeur(3).getCouleur())<0) {
							joueur11 = j;
						}
					}
				}
				
				joueur11.ajouterJest(c);
				break;
			case PLUS_PETIT_COEUR:
				plusPetite1 = null;
				joueur11 = null;
				
				for(Joueur j : this.joueurs) {
					for(Carte carte : j.getJest().getCartes()) {
						if(carte.getCouleur() == Couleur.COEUR) {
							if(plusPetite1 == null) {
								plusPetite1 = carte;
								joueur11 = j;
							}else if(plusPetite1.getValeurReelle()>carte.getValeurReelle()) {
								plusPetite1 = carte;
								joueur11 = j;
							}
						}
					}
				}
				
				joueur11.ajouterJest(c);
				break;
			case PLUS_GRAND_CARREAU:
				plusFort = null;
				joueur11 = null;
				
				for(Joueur j : this.joueurs) {
					for(Carte carte : j.getJest().getCartes()) {
						if(carte.getCouleur() == Couleur.CARREAU) {
							if(plusFort == null) {
								plusPetite1 = carte;
								joueur11 = j;
							}else if(plusFort.getValeurReelle()<carte.getValeurReelle()) {
								plusPetite1 = carte;
								joueur11 = j;
							}
						}
					}
				}
				
				joueur11.ajouterJest(c);
				break;
			case JOUEUR_MAX_2:
				nbMax = 0;
				joueur11 = null;
				
				for(Joueur j : this.joueurs) {
					int sonMax = 0;
					for(Carte carte : j.getJest().getCartes()) {
						if(carte.getValeur() == 2) {
							sonMax++;
						}
					}
					if(sonMax>nbMax) {
						nbMax = sonMax;
						joueur11 = j;
					}else if(sonMax == nbMax) {
						//alors on va comparer leur meilleure carte de valeur 4
						if(joueur11.meilleureCarteValeur(2).getCouleur().compareTo(j.meilleureCarteValeur(2).getCouleur())<0 && joueur11.meilleureCarteValeur(2) != null && j.meilleureCarteValeur(2).getCouleur() != null) {
							joueur11 = j;
						}
					}
				}
				
				joueur11.ajouterJest(c);
				break;
			case PLUS_GRAND_COEUR:
				plusFort = null;
				joueur11 = null;
				
				for(Joueur j : this.joueurs) {
					for(Carte carte : j.getJest().getCartes()) {
						if(carte.getCouleur() == Couleur.COEUR) {
							if(plusFort == null) {
								plusPetite1 = carte;
								joueur11 = j;
							}else if(plusFort.getValeurReelle()<carte.getValeurReelle()) {
								plusPetite1 = carte;
								joueur11 = j;
							}
						}
					}
				}
				
				joueur11.ajouterJest(c);
				break;
			case PLUS_BAS_CARREAU:
				plusPetite1 = null;
				joueur11 = null;
				
				for(Joueur j : this.joueurs) {
					for(Carte carte : j.getJest().getCartes()) {
						if(carte.getCouleur() == Couleur.CARREAU) {
							if(plusPetite1 == null) {
								plusPetite1 = carte;
								joueur11 = j;
							}else if(plusPetite1.getValeurReelle()>carte.getValeurReelle()) {
								plusPetite1 = carte;
								joueur11 = j;
							}
						}
					}
				}
				
				joueur11.ajouterJest(c);
				break;
			
			default:
				break;
			}
		}
		this.trophees.clear();
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Methode toString
	 * @return Retourne une chaine de caracteres indiquant le nombre de joueurs reels et virtuels, les trophes, les cartes restantes ainsi que les joueurs
	 */
	public String toString() {
		String res = "";
		
		res += "Jeu en cours : \n" + "Joueurs réels = " + this.nbReels + ", Joueurs virtuels = " + this.nbVirtuel;
		res += "\n Trophées : " + this.trophees.toString();
		res += "\nCartes restantes : " + this.cartesRestantes;
		
		//on ajoute les joueurs
		res += "\nVoici les joueurs :" + this.joueurs.toString();
		
		return res;
	}
	
	/**
	 * Recupere le nombre de joueurs reels
	 * @return Retourne un nombre
	 */
	public int getNbReels() {
		return nbReels;
	}

	/**
	 * Set le nombre de joueurs reels
	 * @param nbReels Nombre de joueurs
	 */
	public void setNbReels(int nbReels) {
		this.nbReels = nbReels;
		setChanged();
		notifyObservers();
	}

	/**
	 * Recupere le nombre de joueurs virtuels
	 * @return Retourne un nombre
	 */
	public int getNbVirtuel() {
		return nbVirtuel;
	}

	/**
	 * Set le nombre de joueurs virtuels
	 * @param nbVirtuel Nombre de joueurs
	 */
	public void setNbVirtuel(int nbVirtuel) {
		this.nbVirtuel = nbVirtuel;
		setChanged();
		notifyObservers();
	}

	/**
	 * Recupere la pioche
	 * @return Retourne la Pioche
	 */
	public Pioche getPioche() {
		return pioche;
	}

	/**
	 * Set la pioche
	 * @param pioche Pioche
	 */
	public void setPioche(Pioche pioche) {
		this.pioche = pioche;
		setChanged();
		notifyObservers();
	}

	/**
	 * Recupere la liste des trophes
	 * @return Retourne une liste de cartes
	 */
	public ArrayList<Carte> getTrophees() {
		return trophees;
	}

	/**
	 * Set les trophes
	 * @param trophees Liste de trophes
	 */
	public void setTrophees(ArrayList<Carte> trophees) {
		this.trophees = trophees;
		setChanged();
		notifyObservers();
	}

	/**
	 * Recupere les cartes restantes
	 * @return Retourne une liste de cartes
	 */
	public LinkedList<Carte> getCartesRestantes() {
		return cartesRestantes;
	}

	/**
	 * Set les cartes restantes
	 * @param cartesRestantes Liste de cartes
	 */
	public void setCartesRestantes(LinkedList<Carte> cartesRestantes) {
		this.cartesRestantes = cartesRestantes;
		setChanged();
		notifyObservers();
	}

	/**
	 * Recupere la liste des joueurs
	 * @return Retourne une liste de joueurs
	 */
	public ArrayList<Joueur> getJoueurs() {
		return joueurs;
	}

	/**
	 * Set les joueurs
	 * @param joueurs Liste de joueurs
	 */
	public void setJoueurs(ArrayList<Joueur> joueurs) {
		this.joueurs = joueurs;
		setChanged();
		notifyObservers();
	}

	
	/**
	 * Recupere le compteur de points
	 * @return Retourne le compteur
	 */
	public Visiteur getCompteur() {
		return compteur;
	}

	/**
	 * Set le compteur de points
	 * @param compteur Visiteur
	 */
	public void setCompteur(Visiteur compteur) {
		this.compteur = compteur;
		setChanged();
		notifyObservers();
	}

	/**
	 * Ajoute des cartes mysteres
	 * @param ajout Permet d'ajouter ou non les cartes (true / false)
	 * @param nb Nombre indiquant le nombre de joueurs
	 */
	public void ajouterCarteMystere(boolean ajout, int nb) {
		if(ajout) {
			pioche.ajouterCartesMystere(nb);
		}
	}

	/**
	 * Ajoute un joueur au jeu
	 * @param j Joueur
	 */
	public void ajouterJoueur(Joueur j) {
		this.joueurs.add(j);
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Set le joueur en cours
	 * @param j Joueur
	 */
	public void setJoueurEnCours(Joueur j) {
		this.joueurEnCours = j;
		setChanged();
		notifyObservers();
	}
	
}
