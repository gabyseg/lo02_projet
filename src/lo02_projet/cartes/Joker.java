package lo02_projet.cartes;

import lo02_visiteur.Visiteur;

/**
 * Classe permettant de representer le joker
 * @author gabrielsega
 *
 */
public class Joker extends Carte{
	/**
	 * Constructeur permettant d'instancier une carte joker 
	 * @param cond Condition (trophe) de la carte
	 */
	public Joker(Condition cond, String chemin) {
		super(0, cond, null, chemin);
	}
	
	
	/**
	 * Indique si la carte est un joker
	 */
	public boolean estJoker() {
		return true;
	}
	
	/**
	 * Méthode qui permet d'accepter une viste dans le cas du patron visiteur
	 * @param v visiteur qui visite la carte
	 * @return la valeur de la carte
	 */
	public int accepter(Visiteur v) {
		return v.visiter(this);
	}
}
