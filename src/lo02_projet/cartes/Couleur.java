package lo02_projet.cartes;

/**
 * Ensemble des couleurs existantes
 *
 */
public enum Couleur {
	COEUR, CARREAU, TREFLE, PIQUE
}
