package lo02_projet.cartes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Classe representant la pioche du jeu, les cartes restantes
 *
 */
public class Pioche {
	
	/**
	 * Liste de cartes
	 */
	private LinkedList<Carte> cartes;
	
	/**
	 * Constructeur de la pioche (ajoute toutes les cartes)
	 */
	public Pioche() {
		// TODO Auto-generated constructor stub
		this.cartes = new LinkedList<Carte>();
		
		//on créé toutes les cartes ici
		this.cartes.add(new Carreau(1, Condition.JOUEUR_MAX_4, "ressources/imagesCartes/as_carreau.png"));
		this.cartes.add(new Carreau(2, Condition.PLUS_GRAND_CARREAU, "ressources/imagesCartes/deux_carreau.png"));
		this.cartes.add(new Carreau(3, Condition.PLUS_BAS_CARREAU, "ressources/imagesCartes/trois_carreau.png"));
		this.cartes.add(new Carreau(4, Condition.MEILLEUR_JEST_SANS_JOKER, "ressources/imagesCartes/quatre_carreau.png"));
		
		this.cartes.add(new Coeur(1, Condition.JOKER, "ressources/imagesCartes/as_coeur.png"));
		this.cartes.add(new Coeur(2, Condition.JOKER, "ressources/imagesCartes/deux_coeur.png"));
		this.cartes.add(new Coeur(3, Condition.JOKER, "ressources/imagesCartes/trois_coeur.png"));
		this.cartes.add(new Coeur(4, Condition.JOKER, "ressources/imagesCartes/quatre_coeur.png"));
		
		this.cartes.add(new Trefle(1, Condition.PLUS_FORT_PIQUE, "ressources/imagesCartes/as_pique.png"));
		this.cartes.add(new Trefle(2, Condition.PLUS_PETIT_COEUR, "ressources/imagesCartes/deux_pique.png"));
		this.cartes.add(new Trefle(3, Condition.PLUS_GRAND_COEUR, "ressources/imagesCartes/trois_pique.png"));
		this.cartes.add(new Trefle(4, Condition.PLUS_PETITE_PIQUE, "ressources/imagesCartes/quatre_pique.png"));
		
		this.cartes.add(new Pique(1, Condition.PLUS_FORT_TREFLE, "ressources/imagesCartes/as_trefle.png"));
		this.cartes.add(new Pique(2, Condition.JOUEUR_MAX_3, "ressources/imagesCartes/deux_trefle.png"));
		this.cartes.add(new Pique(3, Condition.JOUEUR_MAX_2, "ressources/imagesCartes/trois_trefle.png"));
		this.cartes.add(new Pique(4, Condition.PLUS_PETITE_TREFLE, "ressources/imagesCartes/quatre_trefle.png"));
		
		//on ajoute le joker
		this.cartes.add(new Joker(Condition.MEILLEUR_JEST, "ressources/imagesCartes/joker.png"));
		
	}
	
	/**
	 * Ajoute une carte mystere par joueur
	 * @param nb Nombre de joueur(s)
	 */
	public void ajouterCartesMystere(int nb) {
		//on rajoute autant de cartes qu'il y a de joueurs
		for(int i=0; i<nb; i++) {
			this.cartes.add(new Mystere("ressources/imagesCartes/carte-interro.png"));
		}
	}
	
	/**
	 * Melange l'ensemble des cartes
	 */
	public void melanger() {
		Collections.shuffle(this.cartes);
	}
	
	/**
	 * Methode toString
	 * @return Retourne une chaine de caracteres contenant l'ensemble des cartes de la pioche
	 */
	public String toString() {
		String res = "Voici la pioche : \n";
		
		res+= this.cartes.toString();
		
		return res;
	}
	
	/**
	 * Tire un nombre de cartes de la pioche
	 * @param nb Nombre de cartes à recuperer
	 * @return Retourne une liste de cartes
	 */
	public LinkedList<Carte> tirer(int nb){
		LinkedList<Carte> l = new LinkedList<Carte>();
		
		while(nb > 0 && this.cartes.size()>0) {
			l.add(this.cartes.pop());
			nb--;
		}
		
		return l;
	}
	
	/**
	 * Tire une carte
	 * @return Retourne une carte de la pioche
	 */
	public Carte tirerUneCarte() {
		return this.cartes.pop();
	}

	/**
	 * Recupere les cartes de la pioche
	 * @return Retourne une liste de cartes
	 */
	public LinkedList<Carte> getCartes() {
		return cartes;
	}

	/**
	 * Set les cartes de la pioches
	 * @param cartes Liste de cartes
	 */
	public void setCartes(LinkedList<Carte> cartes) {
		this.cartes = cartes;
	}
	
}
