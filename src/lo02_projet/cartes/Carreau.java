package lo02_projet.cartes;

import lo02_visiteur.Visiteur;

/**
 * Classe permettant de representer les cartes ayant comme couleur le carreau
 * @author gabrielsega
 *
 */
public class Carreau extends Carte{

	/**
	 * Constructeur qui instancie un objet carreau
	 * @param v Valeur de la carte
	 * @param cond Condition pour obtenir la carte en cas de trophe
	 */
	public Carreau(int v, Condition cond, String chemin) {
		super(v, cond, Couleur.CARREAU, chemin);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Méthode qui permet d'accepter une viste dans le cas du patron visiteur
	 * @param v visiteur qui visite la carte
	 * @return la valeur de la carte
	 */
	public int accepter(Visiteur v) {
		return v.visiter(this);
	}
}
