package lo02_projet.cartes;

/**
 * Ensemble des conditions relatives aux trophes
 *
 */
public enum Condition {
	MEILLEUR_JEST,
	JOKER,
	PLUS_PETITE_TREFLE,
	PLUS_PETITE_PIQUE,
	MEILLEUR_JEST_SANS_JOKER,
	PLUS_FORT_TREFLE,
	PLUS_FORT_PIQUE,
	JOUEUR_MAX_4,
	JOUEUR_MAX_3,
	PLUS_PETIT_COEUR,
	PLUS_GRAND_CARREAU,
	JOUEUR_MAX_2,
	PLUS_GRAND_COEUR,
	PLUS_BAS_CARREAU
}
