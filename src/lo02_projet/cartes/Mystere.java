package lo02_projet.cartes;


import java.awt.Image;
import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;

import lo02_visiteur.Visiteur;

/**
 * Classe de la carte mystere
 *
 */
public class Mystere extends Carte{

	/**
	 * Constructeur de la carte mystere
	 * @param chemin Chemin de l'image de la carte
	 */
	public Mystere(String chemin) {
		super(-1, null, null, chemin);
		// TODO Auto-generated constructor stub
		//on prend une couleur au hasard
		Couleur[] tabCou = Couleur.values();
		int randomNum = ThreadLocalRandom.current().nextInt(0, tabCou.length);
		this.setCouleur(tabCou[randomNum]);
		
		//on prend une condition au hasard
		Condition[] tabCond = Condition.values();
		randomNum = ThreadLocalRandom.current().nextInt(0, tabCond.length);
		this.setCondition(tabCond[randomNum]);
		
		//sa valeur enfin
		randomNum = ThreadLocalRandom.current().nextInt(0, 6 + 1);
		this.setValeur(randomNum);
	}
	
	public int accepter(Visiteur v) {
		return v.visiter(this);
	}
	
	/**
	 * Indique si la carte est noire
	 */
	public boolean estCarteNoire() {
		return true;
	}
	
	/**
	 * Methode toString
	 * @return Retourne "Mystere"
	 */
	public String toString() {
		return "Mystère";
	}
	
	/**
	 * Recupere l'image de la carte visible du joueur en cours
	 * @param joueurEnCours Joueur en train de jouer
	 * @return Retourne l'image
	 * @throws IOException
	 */
	public Image getImage(boolean joueurEnCours) throws IOException {
		// TODO Auto-generated method stub
		Image img = super.getImage(joueurEnCours);
		
		if(joueurEnCours || this.isVisible()) {
			img = img.getScaledInstance((int) (img.getWidth(null)*1.5), (int) (img.getHeight(null)*1.5), Image.SCALE_DEFAULT);
		}
		
		return img;
	}
}
