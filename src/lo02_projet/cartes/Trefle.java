package lo02_projet.cartes;

import lo02_visiteur.Visiteur;

/**
 * Classe permettant de representer les cartes ayant comme couleur le trefle
 *
 */
public class Trefle extends Carte{

	/**
	 * Constructeur qui instancie un objet trefle
	 * @param v Valeur de la carte
	 * @param cond Condition pour obtenir la carte en cas de trophe
	 */
	public Trefle(int v, Condition cond, String chemin) {
		super(v, cond, Couleur.TREFLE, chemin);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Méthode qui permet d'accepter une viste dans le cas du patron visiteur
	 * @param v visiteur qui visite la carte
	 * @return la valeur de la carte
	 */
	public int accepter(Visiteur v) {
		return v.visiter(this);
	}

	/**
	 * Méthode qui permet de savoir si la carte est noire
	 * @return true si la carte est noire
	 */
	public boolean estCarteNoire() {
		return true;
	}
}
