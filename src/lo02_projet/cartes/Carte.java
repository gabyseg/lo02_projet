package lo02_projet.cartes;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import lo02_visiteur.Visite;
import lo02_visiteur.Visiteur;

/**
 * Classe representant une carte du jeu contenant differents attributs
 *
 */
public abstract class Carte implements Visite{
	
	/**
	 * Valeur de la carte
	 */
	private int valeur;
	
	/**
	 * Visibilité de la carte
	 */
	private boolean visible;
	
	/**
	 * Condition (trophe) de la carte
	 */
	private Condition condition;
	
	/**
	 * Couleur de la carte
	 */
	private Couleur couleur;
	
	/**
	 * Chemin de l'image de la carte
	 */
	private String chemin;
	
	/**
	 * Constructeur d'une carte avec tous ses attributs
	 * @param v Valeur de la carte
	 * @param cond Condition (trophe) de la carte
	 * @param c Couleur de la carte (coeur, carreau, pique ou trefle)
	 * @param chemin Chemin de l'image de la carte
	 */
	public Carte(int v, Condition cond, Couleur c, String chemin) {
		this.valeur = v;
		this.visible = false;
		this.condition = cond;
		this.couleur = c;
		this.chemin = chemin;
	}
	
	/**
	 * Methode toString
	 * @return Retourne une chaine de carateres contenant tous les attributs de la carte
	 */
	public String toString(){
		String res = "Carte : ";
		if(this.visible) {
			res += this.valeur + ", visible : " + this.visible + ", condition : " + this.condition + ", couleur : " + this.couleur;
		}else {
			res += "Carte non visible";
		}
		
		return res;
	}
	
	//public abstract int accepter(Visiteur v);
	
	/**
	 * Recupere la valeur reelle d'une carte, les cartes as/5 ont une valeur variable
	 * @return Retourne une valeur
	 */
	public int getValeurReelle() {
		int vr = this.valeur;
		if(valeur == 1) {
			vr = 5;
		}
		
		return vr;
	}
	
	/**
	 * Indique si la carte est un joker
	 * @return Retourne true ou false
	 */
	public boolean estJoker() {
		return false;
	}
	
	/**
	 * Indique si la carte est noire
	 * @return Retourne true ou false
	 */
	public boolean estCarteNoire() {
		return false;
	}
	
	/**
	 * Recupere la valeur de la carte
	 * @return Retourne une valeur
	 */
	public int getValeur() {
		return valeur;
	}

	/**
	 * Set la valeur de la carte
	 * @param valeur Valeur de la carte
	 */
	public void setValeur(int valeur) {
		this.valeur = valeur;
	}

	/**
	 * Indique si la carte est visible ou non
	 * @return Retourne true ou false
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * Set la visibilite de la carte
	 * @param visible Visibilite (true ou false)
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
	 * Recupere la condition (trophe) de la carte
	 * @return Retourne une Condition
	 */
	public Condition getCondition() {
		return condition;
	}

	/**
	 * Set la condition de la carte
	 * @param condition Condition
	 */
	public void setCondition(Condition condition) {
		this.condition = condition;
	}
	
	/**
	 * Recupere la couleur de la carte
	 * @return Retourne une Couleur
	 */
	public Couleur getCouleur() {
		return couleur;
	}

	/**
	 * Set la couleur de la carte
	 * @param couleur Couleur
	 */
	public void setCouleur(Couleur couleur) {
		this.couleur = couleur;
	}

	/**
	 * Recupere le chemin de l'image de la carte
	 * @return Retourne un chemin
	 */
	public String getChemin() {
		return chemin;
	}

	/**
	 * Set le chemin de l'image de la carte
	 * @param chemin Chemin de la l'image
	 */
	public void setChemin(String chemin) {
		this.chemin = chemin;
	}
	
	/**
	 * Recupere l'image de la carte visible du joueur en cours
	 * @param joueurEnCours Joueur en train de jouer
	 * @return Retourne l'image
	 * @throws IOException
	 */
	public Image getImage(boolean joueurEnCours) throws IOException {
		Image img = null;
		
		if(this.visible == true || joueurEnCours) {
			img = ImageIO.read(new File(this.chemin));
		}else {
			img = ImageIO.read(new File("ressources/imagesCartes/dosCarteJest.png"));
		}
				
				
		return img;
	}
	
}
