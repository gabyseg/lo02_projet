package lo02_projet.cartes;

import java.util.ArrayList;
import java.util.ListIterator;

import lo02_visiteur.Visite;
import lo02_visiteur.Visiteur;

/**
 * Classe representant les cartes recuperees de chaque joueur
 *
 */
public class Jest implements Visite{
	
	/**
	 * Liste de cartes
	 */
	private ArrayList<Carte> cartes;
	
	/**
	 * Constructeur du Jest
	 */
	public Jest() {
		// TODO Auto-generated constructor stub
		this.cartes = new ArrayList<Carte>();
	}
	
	/**
	 * Ajoute une carte au Jest
	 * @param c Carte à ajouter
	 */
	public void ajouterCarte(Carte c) {
		this.cartes.add(c);
	}
	
	/**
	 * Methode toString
	 * @return Retourne une chaine de caracteres contenant l'ensemble des cartes du Jest
	 */
	public String toString() {
		return cartes.toString();
	}

	/**
	 * Recupere les cartes du Jest
	 * @return Retourne une liste de cartes
	 */
	public ArrayList<Carte> getCartes() {
		return cartes;
	}

	/**
	 * Set les cartes du Jest
	 * @param cartes Liste de cartes
	 */
	public void setCartes(ArrayList<Carte> cartes) {
		this.cartes = cartes;
	}
	
	/**
	 * Compte le nombre de coeur contenu dans le Jest
	 * @return Retourne le nombre de coeur present dans le Jest
	 */
	public int nombreCoeurs() {
		int nb = 0;
		
		for(Carte c : this.cartes) {
			if(c.getCouleur() == Couleur.COEUR) {
				nb++;
			}
		}
		
		return nb;
	}
	
	/**
	 * Indique si le Jest contient un Joker
	 * @return Retourne true ou false
	 */
	public boolean aUnJoker() {
		boolean joker = false;
		
		ListIterator<Carte> it = this.cartes.listIterator();
		while(joker == false && it.hasNext()) {
			Carte c = it.next();
			joker = c.estJoker();
		}
		
		return joker;
	}

	@Override
	public int accepter(Visiteur v) {
		// TODO Auto-generated method stub
		return v.visiter(this);
	}
	
	
}
