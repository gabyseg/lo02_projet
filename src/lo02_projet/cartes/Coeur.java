package lo02_projet.cartes;

import lo02_visiteur.Visiteur;

/**
 * Classe permettant de representer les cartes ayant comme couleur le coeur
 *
 */
public class Coeur extends Carte{

	/**
	 * Constructeur qui instancie un objet coeur
	 * @param v Valeur de la carte
	 * @param cond Condition pour obtenir la carte en cas de trophe
	 */
	public Coeur(int v, Condition cond, String chemin) {
		super(v, cond, Couleur.COEUR, chemin);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Methode qui permet d'utiliser le patron visiteur et d'accepter la visite
	 */
	public int accepter(Visiteur v) {
		return v.visiter(this);
	}
}
