package lo02_projet.strategies;

import java.util.ArrayList;
import java.util.Random;

import lo02_projet.cartes.Carte;
import lo02_projet.joueur.Joueur;

/**
 * Classe correspondant a une strategie de jeu aleatoire
 *
 */
public class Aleatoire implements StrategieBots{

	/**
	 * Methode permettant la selection du joueur auquel prendre une carte
	 * @param liste Liste des joueurs
	 * @return Retourne un joueur
	 */
	public Joueur choisirJoueur(ArrayList<Joueur> liste) {
		// TODO Auto-generated method stub
		Random r = new Random();
		
		//un joueur au hasard
		int choix = r.nextInt(liste.size());
		Joueur j = liste.get(choix);
		
		return j;
	}

	/**
	 * Methode permettant la selection de la carte a recuperer
	 * @param j Joueur auquel prendre la carte
	 * @return Retourne une carte
	 * @throws Exception
	 */
	public Carte choisirCarte(Joueur j) throws Exception {
		// TODO Auto-generated method stub
		//on prend un chiffre au hasard
		Random r = new Random();
		int chiffre = r.nextInt(2);
		
		//boolean pour savoir si on prend la carte visible ou non
		boolean visible = false;
		if(chiffre == 1)
			visible = true;
		
		//on prend la carte du joueur
		Carte c = j.donnerCarte(visible);
		
		return c;
	}

	/**
	 * Methode permettant d'effectuer une offre en retournant l'index d'une des cartes de l'offre (0 ou 1)
	 * @param cartes Cartes de l'offre
	 * @return Retourne 0 ou 1
	 */
	public int faireOffre(Carte[] cartes) {
		// TODO Auto-generated method stub
		//on retourne une des deux cartes au hasard
		Random r = new Random();
		int chiffre = r.nextInt(2);
		return chiffre;
	}

}
