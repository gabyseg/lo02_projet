package lo02_projet.strategies;

import java.util.ArrayList;

import lo02_projet.cartes.Carte;
import lo02_projet.joueur.Joueur;

/**
 * Strategie des joueurs virtuels
 *
 */
public interface StrategieBots {
	
	/**
	 * Methode permettant la selection du joueur auquel prendre une carte
	 * @param liste Liste des joueurs
	 * @return Retourne un joueur
	 */
	public Joueur choisirJoueur(ArrayList<Joueur> liste);
	
	/**
	 * Methode permettant la selection de la carte a recuperer
	 * @param j Joueur auquel prendre la carte
	 * @return Retourne une carte
	 * @throws Exception
	 */
	public Carte choisirCarte(Joueur j) throws Exception;
	
	/**
	 * Methode permettant d'effectuer une offre en retournant l'index d'une des cartes de l'offre (0 ou 1)
	 * @param cartes Cartes de l'offre
	 * @return Retourne 0 ou 1
	 */
	public int faireOffre(Carte[] cartes);
}
