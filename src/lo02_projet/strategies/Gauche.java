package lo02_projet.strategies;

import java.util.ArrayList;
import java.util.Random;

import lo02_projet.cartes.Carte;
import lo02_projet.joueur.Joueur;

/**
 * Classe correspondant a une stragegie de jeu dans laquelle le joueur virtuel rend toujours la carte de gauche (0) visible et prend la carte non visible du premier joueur de la liste
 *
 */
public class Gauche implements StrategieBots{

	/**
	 * Methode permettant la selection du premier joueur de la liste auquel prendre une carte
	 * @param liste Liste des joueurs
	 * @return Retourne un joueur
	 */
	public Joueur choisirJoueur(ArrayList<Joueur> liste) {
		// TODO Auto-generated method stub
		return liste.get(0);
	}

	/**
	 * Methode permettant la selection de la carte non visible
	 * @param j Joueur auquel prendre la carte
	 * @return Retourne une carte
	 * @throws Exception
	 */
	public Carte choisirCarte(Joueur j) throws Exception {
		// TODO Auto-generated method stub		
		//on prend la carte du joueur
		Carte c = j.donnerCarte(false);
		
		return c;
	}

	/**
	 * Methode permettant d'effectuer une offre en retournant la carte de gauche (0)
	 * @param cartes Cartes de l'offre
	 * @return Retourne 0 ou 1
	 */
	public int faireOffre(Carte[] cartes) {
		// TODO Auto-generated method stub
		//on retourne une des deux cartes au hasard
		return 0;
	}

}
