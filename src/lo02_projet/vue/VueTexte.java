package lo02_projet.vue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import lo02_projet.jeu.Jeu;
import lo02_projet.joueur.Joueur;

/**
 * Classe permettant la gestion de l'affichage console
 *
 */
public class VueTexte implements Runnable, Observer{
	
	private Joueur ancien;
	
	/**
	 * Jeu en cours
	 */
	private Jeu j;
	
	/**
	 * Constructeur de la vue
	 * @param j Jeu en cours
	 */
	public VueTexte(Jeu j) {
		// A completer
		j.addObserver(this);
		this.ancien = null;
		this.j = null;

		Thread t = new Thread(this);
		t.start(); 
	}

	@Override
	public void run() {
		
	}
	
	/**
	 * Affichage des cartes du joueur afin de selectionner celle a rendre visible
	 * @param joueur Joueur en train de jouer
	 */
	public void afficherChoixRetournerCarte(Joueur joueur) {
		System.out.println(joueur.getNom() + " : Choississez la carte que vous souhaitez rendre visible :");
		System.out.println("1 - " + joueur.getOffre()[0]);
		System.out.println("2 - " + joueur.getOffre()[1] + "\n");
	}
	
	/**
	 * Affichage des joueurs afin de selectionner celui a qui l'on veut prendre une carte
	 * @param jou Joueur en train de jouer
	 */
	public void afficherJouEnCoursDeJeu(Joueur jou) {
		System.out.println(jou.getNom() + " : Vous devez choisir le joueur dont vous voulez prendre la carte : ");
		ArrayList<Joueur> ar = this.j.getJoueursPiochables();
		if(ar.size() != 1)
			ar.remove(jou);
		System.out.println(ar + "\n");
		System.out.println("Tapez son nom");
		System.out.println("Puis v ou nv pour visible et non visible");
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		if(o instanceof Jeu) {
			Jeu j = (Jeu)o;
			
			if(this.j == null) {
				for(Joueur joueuruu : j.getJoueurs()) {
					joueuruu.addObserver(this);
				}
			}
		
			if(j.getJoueurEnCours() != null  && j.getJoueurEnCours().estReel()) {
				this.afficherChoixRetournerCarte(j.getJoueurEnCours());
			}
			ancien = j.getJoueurEnCours();
			this.j = j;
		}else if(o instanceof Joueur) {
			Joueur jou = (Joueur)o;
			if(jou.isEstEnCoursDeJeu() && jou.estReel() && this.j.getJoueursPiochables().size() != 0) {
				this.afficherJouEnCoursDeJeu(jou);
			}
		}
	}

}
