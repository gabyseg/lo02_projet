package lo02_projet.vue;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.TextArea;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import lo02_projet.cartes.Carte;
import lo02_projet.controleur.Collecteur;
import lo02_projet.controleur.ControleurCarte;
import lo02_projet.controleur.ControleurDepotCarte;
import lo02_projet.controleur.EntreeAttendue;
import lo02_projet.controleur.MontreJestControleur;
import lo02_projet.jeu.Jeu;
import lo02_projet.joueur.Joueur;
/**
 * Classe qui permet d'afficher le jeu
 * @author gabrielsega
 *
 */
public class VueJeu extends JPanel implements Observer{
	
	private ArrayList<JPanel> cases;
	
	/**
	 * Jeu en cours
	 */
	private Jeu j;
	
	private JTextArea instructions;

	/**
	 * Constructeur de la vue
	 * @param j jeu à afficher
	 * @throws IOException
	 */
	public VueJeu(Jeu j) throws IOException {
		super();
		this.j = j;

		//on fait observer le jeu
		j.addObserver(this);
		//et les joueurs
		for(Joueur joueur : this.j.getJoueurs()) {
			joueur.addObserver(this);
		}

		//On ajoute le layout
		this.setLayout(new GridLayout(3, 3));

		//on créé les cases
		this.cases = new ArrayList<JPanel>();

		for(int i=0; i<3; i++) {
			for(int k=0; k<3; k++) {
				JPanel jp = new JPanel();
				jp.setLayout(new BorderLayout());
				this.add(jp);

				if((i+k)%2 == 1 || (i ==1 && k==1)) {
					this.cases.add(jp);
				}
			}
		}

		//on met le panneau central à la toute fin
		JPanel temp = this.cases.remove(2);
		this.cases.add(temp);

		//on dessine les joueurs
		dessinerJoueurs();
		//les trophées
		JTextArea textArea = new JTextArea("", 6, 16);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        textArea.setOpaque(false);
        textArea.setEditable(false);
        this.instructions = textArea;
        
        dessinerTrophees();
	}

	//@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		dessinerJoueurs();
		try {
			dessinerTrophees();
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}
	}
	
	/**
	 * Méthode permettant d'afficher les trophées
	 * @throws IOException
	 */
	public void dessinerTrophees() throws IOException {
		//on récupère le JPanel des Trophées
		JPanel jp = this.cases.get(this.cases.size()-1);
		
		//on l'efface
		jp.removeAll();
		jp.revalidate();
		jp.repaint();
		
		jp.setLayout(new BorderLayout());
				
		//on ajoute les trophées
		ArrayList<Carte> listeC = j.getTrophees();

		JPanel jTemp = new JPanel();
		for(Carte c : listeC) {
			Image img = c.getImage(true);
			img = img.getScaledInstance(img.getWidth(null)/3, img.getHeight(null)/3, Image.SCALE_DEFAULT);
			JLabel label = new JLabel(new ImageIcon(img));
			jTemp.add(label);
		}
		
		jp.add(jTemp, BorderLayout.WEST);
		//on créé un nouveau Jpanel d'instructions
		JPanel panelInstr = new JPanel();
		panelInstr.setLayout(new GridLayout(1, 1));
		panelInstr.add(this.instructions);
		dessinerJoueurs();
        
		jp.add(panelInstr, BorderLayout.EAST);
		jp.repaint();
		
	}

	/**
	 * Méthode permettant de dessiner les joueurs
	 */
	public void dessinerJoueurs() {
		//ici on dessine tous joueurs
		ArrayList<Joueur> l = this.j.getJoueurs();
		//on prend les joueurs piochable
		ArrayList<Joueur> jouPiochables = this.j.getJoueursPiochables();
		for(int i=0; i<l.size(); i++) {
			//on récupère le JPanel
			JPanel jp = this.cases.get(i);
			Joueur jou = l.get(i);
			
			//on l'efface
			jp.removeAll();
			jp.revalidate();
			jp.repaint();

			//on construit le panel
			//on ajoute le nom du joueur
			JPanel t = new JPanel();
			t.setLayout(new GridBagLayout());
			JLabel label = new JLabel(jou.getNom(), SwingConstants.CENTER);
			t.add(label);
			//le bouton pour voir le jest
			if(jou.isEstEnCoursDeJeu() || jou == j.getJoueurEnCours()) {
				JButton jb = new JButton("Voir jest");
				if(jou.getJest().getCartes().size() == 0) {
					jb.setEnabled(false);
				}
				new MontreJestControleur(jb, jou);
				t.add(jb);
			}			
			
			jp.add(t, BorderLayout.NORTH);
			
			//on va ajouter les cartes de son offre
			Carte[] offre = jou.getOffre();
			JPanel jtemp = new JPanel();
			for(int k=0; k<offre.length; k++) {
				Carte c = offre[k];
				if(c != null) {
					//si un joueur joue, on modifie les cartes en fonction
					if(jouPiochables.contains(jou) && this.j.getJoueurEnCours() == null && jou.isEstEnCoursDeJeu() == false) {
						this.dessinerPiocher(c, jtemp, jou);
					}else if(jouPiochables.size() == 1 && jou.isEstEnCoursDeJeu() && jouPiochables.contains(jou)){
						//si il ne peut piocher que dans son tas
						this.dessinerPiocher(c, jtemp, jou);
					}else {
						this.dessinerCarteOffre(c, jtemp, jou == this.j.getJoueurEnCours(), k);
					}
						
				}
			}
			jp.add(jtemp);
		}
	}
	
	/**
	 * Méthode qui permet de dessiner une des cartes d'une offre
	 * @param c la carte
	 * @param jp jpannel concerné
	 * @param rendreVisible booléen pour savoir si la carte doit être visible
	 * @param numDansOffre numéro de la carte dans l'offre
	 */
	public void dessinerCarteOffre(Carte c, JPanel jp, boolean rendreVisible, int numDansOffre) {
		JButton button = new JButton();
		  try {
		    Image img = c.getImage(rendreVisible);
		    img = img.getScaledInstance(img.getWidth(null)/3, img.getHeight(null)/3, Image.SCALE_DEFAULT);

		    button.setIcon(new ImageIcon(img));
		    button.setEnabled(rendreVisible);
		    
		    new ControleurCarte(button, c, numDansOffre);
		  } catch (Exception ex) {
		    //System.out.println(ex);
		  }
		jp.add(button);
	}
	
	/**
	 * Méthode qui permet de dessiner les carte lorsque qu'un p$joueur en prend une aux autres
	 * @param c carte concerneée
	 * @param jp Jpanel concerné
	 * @param j joueur concerné
	 */
	public void dessinerPiocher(Carte c, JPanel jp, Joueur j) {
		//si on attends le nom d'un joueur
		if(true) {
			JButton button = new JButton();
			  try {
			    Image img = c.getImage(c.isVisible());
			    img = img.getScaledInstance(img.getWidth(null)/3, img.getHeight(null)/3, Image.SCALE_DEFAULT);

			    button.setIcon(new ImageIcon(img));
			    //pour savoir si on affiche tout ou seulement les cartes d'un joueur
			    button.setEnabled(Collecteur.getInstance().getAttendue() == EntreeAttendue.NOM_JOUEUR_PRENDRE_CARTE);
			    
			    //listener
			    new ControleurDepotCarte(button, c, j);
			  } catch (Exception ex) {
			    //System.out.println(ex);
			  }
			jp.add(button);
		}
	}
	
	/**
	 * Méthode qui permet d'afficher les scores
	 */
	public void changerScores() {
		//on l'efface
		this.removeAll();
		this.revalidate();
		this.repaint();
		
		//on change le layout
		this.setLayout(new GridBagLayout());
		
		//on créé la zone de texte
		JTextArea textArea = new JTextArea("", 6, 15);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        textArea.setOpaque(false);
        textArea.setEditable(false);
        textArea.setText(this.j.getScores());

        //on change la police
        textArea.setFont(textArea.getFont().deriveFont(20f)); // will only change size to 12pt
        
        //on ajoute le tout
        this.add(textArea);
        textArea.setFont(textArea.getFont().deriveFont(20f));
        this.repaint();
        textArea.setFont(textArea.getFont().deriveFont(20f));

	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		repaint();
		if(o instanceof Joueur) {
			Joueur j = (Joueur)o;
			if(j.isEstEnCoursDeJeu()) {
				this.instructions.setText(j.getNom() + " vous devez choisir une carte parmis celles disponibles");
			}else {
				if(this.instructions != null) {
					this.instructions.setText("");
				}
			}
		}else if(o instanceof Jeu) {
			Jeu j = (Jeu)o;
			System.out.println("Voici le jeu " + j.toString() + "\n\n\n\n");
			if(j.getJoueurEnCours() != null) {
				this.instructions.setText(j.getJoueurEnCours().getNom() + " vous devez choisir quelle carte vous voulez retourner");
			}else if(j.getEstFini()) {
				this.changerScores();
			}
		}
	}

}
