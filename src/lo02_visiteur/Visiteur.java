package lo02_visiteur;

import lo02_projet.cartes.Carreau;
import lo02_projet.cartes.Carte;
import lo02_projet.cartes.Coeur;
import lo02_projet.cartes.Jest;
import lo02_projet.cartes.Joker;
import lo02_projet.cartes.Mystere;
import lo02_projet.cartes.Pique;
import lo02_projet.cartes.Trefle;

/**
 * Interface qui permet d'implémenter le patron visiteur
 * @author gabrielsega
 *
 */
public interface Visiteur {
	public int visiter(Coeur c);
	public int visiter(Carreau c);
	public int visiter(Pique p);
	public int visiter(Trefle t);
	public int visiter(Joker j);
	public int visiter(Jest j);
	public int visiter(Mystere mystere);
}
