package lo02_visiteur;

/**
 * Interface permettant d'implémenter le patron visiteur
 * @author gabrielsega
 *
 */
public interface Visite {
	public int accepter(Visiteur v);
}
